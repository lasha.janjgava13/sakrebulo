$(document).ready(function() {
    $('.poll-activate').click(function(event) {
        event.preventDefault();
        var $el = $(this);
        if ($el.hasClass('disabled')) {
            return;
        }
        var id = $el.data('id');
        $.ajax({
            method: 'get',
            url: '/admin/polls/activate/' + id,
            success: function(data) {
                $('.poll-activate[data-id=' + data.old + ']')
                    .removeClass('btn-default disabled')
                    .addClass('btn-primary');
                $el.addClass('btn-default disabled');
            },
        });
    });
});
