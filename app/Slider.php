<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Slider extends Model
{
    use Cachable;

    // protected $cacheCooldownSeconds = 3600;
    protected $cacheCooldownSeconds = 0;

    protected $dates = ['date', 'created_at', 'updated_at'];
}
