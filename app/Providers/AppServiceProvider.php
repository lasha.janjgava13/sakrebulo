<?php

namespace App\Providers;

use Voyager;
use App\Team;
use App\Partner;
use Carbon\carbon;
use App\Tournament;
use App\Translation;
use App\CustomActions\ActivatePoll;
use App\CustomActions\Matches;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Video;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Voyager::addAction(Matches::class);
        Voyager::addAction(ActivatePoll::class);
        Voyager::addAction(\App\Actions\MyAction::class);
        Voyager::useModel('Translation', Translation::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if (!App::runningInConsole()) {
            $partners = Partner::withCacheCooldownSeconds()->where('active', 1)->orderBy('priority', 'desc')->get();
            View::composer('*', function ($view) use ($partners) {
                return $view->with([
                    'locale' => app()->getLocale(),
                    'fallbackLocale' => config('app.fallback_locale'),
                    'partners' => $partners,
                ]);
            });
            $this->addSidebarVars();
        }
    }

    protected function addSidebarVars()
    {
        $video = null;
        $videoId = setting('site.sidebar_video');
        if (!is_null($videoId)) {
            $video = Video::where('active', 1)->find($videoId);
        }
        View::composer('partials.sidebar', function ($view) use ($video) {
            return $view->with([
                'sidebarVideo' => $video
            ]);
        });
    }
}
