<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AdditionalGroup extends Model
{
    $protected $table = 'AdditionalGroups';
}
