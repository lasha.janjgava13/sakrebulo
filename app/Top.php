<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Top extends Model
{
    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function type()
    {
        return $this->belongsTo(Toptype::class);
    }
}
