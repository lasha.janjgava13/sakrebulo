<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Region extends Model
{
    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
