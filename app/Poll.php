<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Poll extends Model
{
    use Translatable;
    //    use Cachable;

    protected $translatable = ['title'];

    protected $cacheCooldownSeconds = 600;

    public function type()
    {
        return $this->belongsTo(PollType::class);
    }

    public function answers()
    {
        return $this->HasMany(PollAnswer::class);
    }

    public function players()
    {
        return $this->belongsToMany(Player::class, 'poll_questions', 'poll_id', 'player_id')->withPivot('team_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'poll_questions', 'poll_id', 'team_id')->withPivot('player_id');
    }
    public function hasPlayers()
    {
        return (bool) $this->players()->first();
    }
    public function pollMax($option,$player)
    {
        $poll_answers = $this->answers()->get();
        $poll_questions = $this->$option()->get();
        $poll_counts = [];
        foreach ($poll_questions as $v) {
            $count = $poll_answers->where('answer_id', $v->id)->count();
            if (!isset($poll_counts[$v->id])) {
                $poll_counts[$v->id]['answers_count'] = $count;
            }
            if($player->id == $v->id){
                $player_st = $count;
            }
        }
        $max = collect($poll_counts)->max();
        return $max > $player_st ? false : true;
    }
}
