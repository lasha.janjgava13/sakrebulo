<?php

namespace App\CustomActions;

use TCG\Voyager\Actions\AbstractAction;

class Matches extends AbstractAction
{
  public function getTitle()
       {
           return 'Manage';
       }

       public function getIcon()
       {
           return 'voyager-edit';
       }

       public function getAttributes()
       {
           return [
               'class' => 'btn btn-sm btn-primary pull-right edit'
           ];
       }

       public function getDefaultRoute()
       {
           return route('voyager.' . $this->dataType->slug . '.manage', $this->data->{$this->data->getKeyName()});
       }

       public function getDataType()
       {
         return 'matches';
       }
}



 ?>
