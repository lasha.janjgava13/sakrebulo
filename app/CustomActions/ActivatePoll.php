<?php

namespace App\CustomActions;

use TCG\Voyager\Actions\AbstractAction;

class ActivatePoll extends AbstractAction
{
    public function getTitle()
    {
        return 'Activate poll';
    }

    public function getIcon()
    {
        return 'voyager-check';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        $data = json_decode($this->data);
        $id = $data->id;
        $additionalClasses = in_array($id, [setting('site.players_poll_id'), setting('site.teams_poll_id')]) ? ' btn-default disabled' : ' btn-primary';
        return [
            'data-id' => $id,
            'class' => 'btn btn-sm pull-left poll-activate' . $additionalClasses,
        ];
    }

    public function getDefaultRoute()
    {
        return '#';
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'polls';
    }
}
