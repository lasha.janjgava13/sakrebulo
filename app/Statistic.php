<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Statistic extends Model
{
    #use Cachable;

    protected $table = 'statistic';

    protected $fillable = ['player_id', 'match_id', 'value'];


    public function player()
    {
        return $this->belongsTo(Player::class);
    }


    public function match()
    {
        return $this->belongsTo(Match::class);
    }
}
