<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Player;
use App\Tournament;
use Carbon\Carbon;
Use App\Statistic;
Use App\Highlight;
use App\Team;
use DB;

class StatisticCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:StatisticCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache Statistic For Each Player';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function saveToDb($data){
        foreach($data as $keyval=> $typeArray){
            foreach($typeArray as $key=>$player){
                DB::table('StatisticBuild')->insert([
                    'type'=>$keyval,
                    'player_id'=>$player['player']->id,
                    'value'=>$player['value'],
                    'created_at'=>Carbon::now()
                ]);
            }
        }

    }
    public function DropStatistic(){
        DB::table('StatisticBuild')->truncate();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $take = 20;
        $date = Carbon::now();
        
           $date = Carbon::now();
        if (config('app.tournament')) {
            $Tournament = config('app.tournament');
        } else {
            $Tournament = Tournament::whereRaw(
                "end_date >= ? AND start_date <= ?",
                array($date, $date)
            )->first();
        }

        $this->line($Tournament->id);

        $players = $Tournament->groups->map(function ($group) {
            return $group->teams->where('active', 1)->map(function ($team) {
                return $team->players;
            });
        })->flatten();
        $Tournament = $Tournament::with('groups','groups.teams','groups.teams.homegames')->where('id',$Tournament->id)->first();

        $matches = $Tournament->groups->map(function($group){
            return $group->teams->where('active','1')->map(function($team){
                return $team->homegames()->get();
            });
        })->flatten();
        $matches = $matches->map(function ($match) {
            return $match->id;
        });
        $match_ids = $matches->toarray();
        
        
        $statistic = Statistic::whereIn('match_id',$match_ids)->where('value','!=','0')->get();
        $player_statistic_array = [];
        $num = $statistic->groupBy('player_id')->map(function ($row) {
            return array('count' =>$row->count() ,'value'=>$row->sum('value'));
        });

        $player_highlight_array = [];
        $highlights = Highlight::whereIn('match_id',$match_ids)->get();
        $highlights = $highlights->groupBy('player_id')->map(function ($row) {
            return $row;
        });

        $topGoal = [];
        $topAssist = [];
        $topPlayer = [];
        foreach ($players as $player) {
            $topGoal[$player->id]['player'] = $player;
            $topGoal[$player->id]['value'] = 0;
            $topAssist[$player->id]['player'] = $player;
            $topAssist[$player->id]['value'] = 0;
            if(isset($num[$player->id])){
                $sum = $num[$player->id]['value'];
                $count = $num[$player->id]['count'];
            }else{
                $count = 0;
            }
            if ($count) {
                $topPlayer[$player->id]['player'] = $player;
                $topPlayer[$player->id]['value'] = number_format($sum / $count, 1);
            } else {
                $topPlayer[$player->id]['player'] = $player;
                $topPlayer[$player->id]['value'] = 0;
            }
            if(isset($highlights[$player->id])){
                 foreach ($highlights[$player->id] as $value) {
                    switch ($value->type_id) {
                        case '2':
                            $topGoal[$player->id]['value']++;
                            break;
                        case '3':
                            $topAssist[$player->id]['value']++;
                            break;
                    }
                  }
            }
            $topPlayer[$player->id]['goals'] = $topGoal[$player->id]['value'];
        }

        $topGoal = collect($topGoal)->sortBy('value')->reverse()->take($take);
        $topAssist = collect($topAssist)->sortBy('value')->reverse()->take($take);
        $topPlayer = collect($topPlayer)->sortBy('value')->reverse()->take($take);
        $this->DropStatistic();
        $this->saveToDb([
            'player' => $topPlayer,
            'goaler' => $topGoal,
            'assistant' => $topAssist
        ]);
    }
}
