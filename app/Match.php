<?php

namespace App;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Match extends Model
{
    use Compoships, Translatable;

    protected $translatable = ['stadium'];

    public function homeId()
    {
        return $this->belongsTo(Team::class, 'home_id', 'id');
    }

    public function awayId()
    {
        return $this->belongsTo(Team::class,  'away_id', 'id');
    }

    public function highlight()
    {
        return $this->hasMany(Highlight::class);
    }

    public function MatchType()
    {
        return $this->belongsTo(MatchType::class);
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function home_squad()
    {
        return $this->belongsToMany(Player::class, 'match_players_home');
    }
    public function away_squad()
    {
        return $this->belongsToMany(Player::class, 'match_players_away');
    }
}
