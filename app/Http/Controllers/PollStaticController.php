<?php

namespace App\Http\Controllers;

use Auth;
use App\Poll;
use App\PollAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PollStaticController extends Controller
{
    public static function getActivePolls(Request $request)
    {
        $polls = [];
        $poll = Poll::with('players', 'teams')->get();
        foreach ($poll as $key => $value) {
            if ($value->type_id == 1) {
                $option = 'teams';
            } elseif ($value->type_id == 2) {
                $option = 'players';
            } else {
                $option = 'invalid';
            }
            $poll_counts = [];
            $poll_questions = $value->$option()->get();
            $poll_answers = $value->answers()->get();
            $poll_answers_count = $poll_answers->count();

            foreach ($poll_questions as $v) {
                $count = $poll_answers->where('answer_id', $v->id)->count();
                if (!isset($poll_counts[$v->id])) {
                    $poll_counts[$v->id]['answers_count'] = $count;
                }
                $poll_counts[$v->id]['percent'] = $poll_answers_count ? number_format(($count / $poll_answers_count) * 100, 1) : 0;
            }
            $time = Carbon::now();
            #$can = $value->answers()->where('ip', request()->ip())->first();

            $can =  PollAnswer::where('poll_id', $value->id)->where('ip', request()->ip())->orderBy('polled','DESC')->first();

            if ($can == null || Carbon::parse($can->polled)->diff(Carbon::parse($time))->format('%d') >= 1) {
                $can = true;
            } else {
                $can = false;
            }
            // if($request->hasCookie('poll_' . $value->id)) {
            //    $can = false;
            // }else{
            //     $can = true;
            // }
            $polls[$option] = [
                'item' => $value,
                'count' => $poll_answers_count,
                'questions' => $poll_counts,
                'can' => $can,
            ];
        }
        return (object) $polls;
    }

    public function ajax_getActivePolls(Request $request, $id)
    {
        $poll = Poll::with('Players', 'teams')->where('id', $id)->firstOrFail();
        if ($request->has('locale')) {
            app()->setLocale($request->get('locale'));
        }
        $lang = app()->getLocale();
        if ($poll->type_id == 1) {
            $option = 'teams';
        } elseif ($poll->type_id == 2) {
            $option = 'players';
        } else {
            $option = 'invalid';
        }
        $poll_counts = [];
        $poll_questions = $poll->$option()->get();
        $cds = 0;
        foreach($poll_questions as $pl){
            $cds += PollAnswer::where('answer_id',$pl->id)->where('poll_id',$id)->count();
        }

        $poll_answers_count =$cds;
        foreach ($poll_questions as $v) {
            $count = $poll->answers()->where('answer_id', $v->id)->count();
            if (!isset($poll_counts[$v->id])) {
                $poll_counts[$v->id]['answers_count'] = $count;
                $poll_counts[$v->id]['item'] = (object) [
                    'logo' => ($option == 'teams') ? $v->logo : null,
                    'image' => $v->image,
                    'id' => $v->id,
                    'name' => $v->getTranslatedAttribute('name', $lang),
                    'first_name' => $v->getTranslatedAttribute('first_name', $lang),
                    'last_name' => $v->getTranslatedAttribute('last_name', $lang),
                    'full_name' => $v->getTranslatedAttribute('full_name', $lang),
                ];
            }
            $poll_counts[$v->id]['percent'] = $poll_answers_count ? number_format(($count / $poll_answers_count) * 100, 1) : 0;
        }
        $time = Carbon::now();
        #$can = $poll->answers()->where('ip', request()->ip())->first();
        $can =  PollAnswer::where('poll_id', $poll->id)->where('ip', request()->ip())->orderBy('polled','DESC')->first();
        if ($can == null || Carbon::parse($can->polled)->diff(Carbon::parse($time))->format('%d') >= 1) {
            $can = true;
        } else {
            $can = false;
        }
        // if($request->hasCookie('poll_' . $poll->id)) {
        //    $can = false;
        // }else{
        //     $can = true;
        // }
        return response()->json([
            'payload' => [
                'item' => [
                    'id' => $poll->id,
                    'title' => $poll->getTranslatedAttribute('title', $lang),
                ],
                'count' => $poll_answers_count,
                'questions' => $poll_counts,
                'can' => $can,
            ],
        ]);
    }
}
