<?php

namespace App\Http\Controllers;

use MetaTag;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClubsController extends Controller
{
    public function index()
    {
        $pageTitle = trans('website.teams');
        return view('pages.clubs', compact('pageTitle'));
    }

    public function show($id)
    {
        $team = Team::with('region', 'players')->findOrFail($id);
        $pageTitle = $team->getTranslatedAttribute('name', app()->getLocale());
        MetaTag::set('title', $pageTitle);
        MetaTag::set('image', asset('storage/' . $team->image));

        return view('components.clubs.club', compact('team', 'pageTitle'));
    }
}
