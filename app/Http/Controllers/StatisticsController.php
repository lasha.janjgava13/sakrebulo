<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use Carbon\Carbon;
Use App\Statistic;
Use App\Highlight;
use App\Team;
use App\StatisticBuild;

class StatisticsController extends Controller
{
    public static function getTopPlayers($take = 4)
    {
        $topPlayer = StatisticBuild::with('player')->where('type','player')->orderBy('value','DESC')->limit($take)->get();
        $topGoal = StatisticBuild::with('player')->where('type','goaler')->orderBy('value','DESC')->limit($take)->get();
        $topAssist = StatisticBuild::with('player')->where('type','assistant')->orderBy('value','DESC')->limit($take)->get();
        
        return (object)[
            'player' => $topPlayer,
            'goaler' => $topGoal,
            'assistant' => $topAssist
        ];
    }
}
