<?php

namespace App\Http\Controllers;

use TCG\Voyager\Models\Post;
use Illuminate\Http\Request;
use MetaTag;

class NewsController extends Controller
{
    private $PUBLISHED = 'PUBLISHED';

    /**
     * Display listing of posts
     *
     * @return \Illuminate\View\View;
     */
    public function index(): \Illuminate\View\View
    {
        $news = Post::where('status', $this->PUBLISHED)->latest()->paginate(10);
        $pageTitle = trans('website.news');
        return view('components.news.index', compact('news', 'pageTitle'));
    }

    /**
     * Display single news
     *
     * @return \Illuminate\View\View;
     */
    public function view($slug): \Illuminate\View\View
    {
        $news = Post::where('status', $this->PUBLISHED)->where('slug', $slug)->with('category')->firstOrFail();
        $relatedNews = collect();
        if (is_null($news->category)) {
            $relatedNews = Post::where('status', $this->PUBLISHED)
                ->where('id', '!=', $news->id)
                ->inRandomOrder()
                ->limit(4)
                ->get();
        } else {
            $relatedNews = Post::where('status', $this->PUBLISHED)
                ->where('id', '!=', $news->id)
                ->where('category_id', $news->category->id)
                ->inRandomOrder()
                ->limit(4)
                ->get();
        }
        MetaTag::set('description', mb_substr($news->getTranslatedAttribute('body', app()->getLocale()),0, 200));
        MetaTag::set('image', asset('storage/' . $news->image));
        MetaTag::set('title', $news->getTranslatedAttribute('title', app()->getLocale()));
        $pageTitle = $news->getTranslatedAttribute('title', app()->getLocale());
        return view('components.news.single', compact('news', 'relatedNews', 'pageTitle'));
    }
}
