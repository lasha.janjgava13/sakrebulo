<?php

namespace App\Http\Controllers;

use App\Team;
use App\Match;
use Carbon\Carbon;
use App\Tournament;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function calendar()
    {
        $games = [];
        $date = Carbon::now();
        $tournament = Tournament::with(['groups','groups.teams','groups.teams.games'])->where('active',1)->OrderBy('id','DESC')->first();
        $matches = Match::with(['MatchType', 'awayId', 'homeId', 'awayId.group', 'homeId.group'])->get();

        foreach ($matches as $key => $game) {
            $startDate = \Carbon\Carbon::parse($tournament->start_date);
            $endDate = \Carbon\Carbon::parse($tournament->end_date);
            if(Carbon::parse($game->play_date)->between($startDate,$endDate)){
                $games['schedule'][Carbon::parse($game->play_date)->format('Y-m-d')][] = $game;
            }
         }
        // foreach ($tournament->groups as $key => $group) {
        //     foreach($group->teams as $keyTeams=>$team){
        //         foreach($team->games as $game){
        //             $games['schedule'][Carbon::parse($game->play_date)->format('Y-m-d')][] = $game;
        //         }
        //     }
        // }
        $games['today'] = $date->format('Y-m-d');
        $games['translations'] = [
            'calendar' => trans('calendar'),
            'website' => trans('website')
        ];
        return response()->json($games);
    }

    public function teamplayer($id)
    {
        $team = Team::where('id', $id)->with('players')->first();
        return response()->json($team->players);
    }
}
