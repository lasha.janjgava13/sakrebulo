<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tournament;
use Carbon\Carbon;

class TournamentController extends Controller
{
    public static function getMatches()
    {
        $date = Carbon::now();
        if (config('app.tournament')) {
            $tournament = config('app.tournament');
        } else {
            $tournament = Tournament::whereRaw(
                "end_date >= ? AND start_date <= ?",
                array($date, $date)
            )->first();
        }
        if (is_null($tournament)) {
            return null;
        }
        $groups = $tournament->groups()->with('teams', 'teams.homegames', 'teams.awaygames')->get();

        $tournament = [];

        foreach ($groups as $group) {
            $teams_profiles = [];
            $team_games = [];
            $tournament[$group->id]['group'] = $group;
            foreach ($group->teams as $team) {
                $teams_profiles[$team->id] = [];
                $teams_profiles[$team->id]['team'] = $team;
                $teams_profiles[$team->id]['goals_out'] = 0;
                $teams_profiles[$team->id]['goals_in'] = 0;
                $teams_profiles[$team->id]['point'] = 0;
                $teams_profiles[$team->id]['wins'] = 0;
                $teams_profiles[$team->id]['loose'] = 0;
                $teams_profiles[$team->id]['draws'] = 0;
                $teams_profiles[$team->id]['goals'] = 0;
                $teams_profiles[$team->id]['last_games'] = [];

                foreach ($team->homegames as $homegame) {
                    if ($homegame->play_off == 1) {
                        continue;
                    }
                    if ($homegame->home_score == null && $homegame->away_score == null) {
                        continue;
                    }

                    $teams_profiles[$team->id]['goals_out'] += $homegame->home_score;
                    $teams_profiles[$team->id]['goals_in'] += $homegame->away_score;

                    $team_games[$team->id][] = $homegame;
                    if ($homegame->home_score > $homegame->away_score) {
                        $teams_profiles[$team->id]['wins']++;
                        $teams_profiles[$team->id]['point'] += 3;
                    } elseif ($homegame->home_score == $homegame->away_score) {
                        $teams_profiles[$team->id]['point'] += 1;
                        $teams_profiles[$team->id]['draws']++;
                    } else {
                        $teams_profiles[$team->id]['loose']++;
                    }
                }
                foreach ($team->awaygames as $awayGames) {
                    if ($awayGames->play_off == 1) {
                        continue;
                    }
                    if ($awayGames->home_score == null && $awayGames->away_score == null) {
                        continue;
                    }

                    $team_games[$team->id][] = $awayGames;
                    if ($awayGames->home_score < $awayGames->away_score) {
                        $teams_profiles[$team->id]['wins']++;
                        $teams_profiles[$team->id]['point'] += 3;
                    } elseif ($awayGames->home_score == $awayGames->away_score) {
                        $teams_profiles[$team->id]['point'] += 1;
                        $teams_profiles[$team->id]['draws']++;
                    } else {
                        $teams_profiles[$team->id]['loose']++;
                    }

                    $teams_profiles[$team->id]['goals_out'] += $awayGames->away_score;
                    $teams_profiles[$team->id]['goals_in'] += $awayGames->home_score;
                }
                $teams_profiles[$team->id]['goals'] = $teams_profiles[$team->id]['goals_out'] - $teams_profiles[$team->id]['goals_in'];
            }
            foreach ($team_games as $key => $value) {
                $tmp = collect($value)->sortBy('play_date')->reverse()->take(5);
                $teams_profiles[$key]['last_games'] = $tmp;
            }
            if ($group->id == 4) {
            }
            //'goals_out')->sortBy('goals')->sortBy('point'
           
            $teams_profiles = collect($teams_profiles)->sortByDesc('point')->groupBy('point')->map(function($groupItems){
                    return $groupItems->sortByDesc('goals');
            });
            $tournament[$group->id]['profiles'] = $teams_profiles->flatten(1);
        }
        return (object)$tournament;
    }
     public static function getMatchesDev()
    {
        $date = Carbon::now();
        if (config('app.tournament')) {
            $tournament = config('app.tournament');
        } else {
            $tournament = Tournament::whereRaw(
                "end_date >= ? AND start_date <= ?",
                array($date, $date)
            )->first();
        }
        if (is_null($tournament)) {
            return null;
        }
        $groups = $tournament->groups()->with('teams', 'teams.homegames', 'teams.awaygames')->get();

        $tournament = [];

        foreach ($groups as $group) {
            $teams_profiles = [];
            $team_games = [];
            $tournament[$group->id]['group'] = $group;
            foreach ($group->teams as $team) {
                $team->points();
                $team->goals();
                $teams_profiles[$team->id] = [];
                $teams_profiles[$team->id]['team'] = $team;
                $teams_profiles[$team->id]['goals_out'] = 0;
                $teams_profiles[$team->id]['goals_in'] = 0;
                $teams_profiles[$team->id]['point'] = 0;
                $teams_profiles[$team->id]['wins'] = 0;
                $teams_profiles[$team->id]['loose'] = 0;
                $teams_profiles[$team->id]['draws'] = 0;
                $teams_profiles[$team->id]['goals'] = 0;
                $teams_profiles[$team->id]['last_games'] = [];

                foreach ($team->homegames as $homegame) {
                    if ($homegame->play_off == 1) {
                        continue;
                    }
                    if ($homegame->home_score == null && $homegame->away_score == null) {
                        continue;
                    }

                    $teams_profiles[$team->id]['goals_out'] += $homegame->home_score;
                    $teams_profiles[$team->id]['goals_in'] += $homegame->away_score;

                    $team_games[$team->id][] = $homegame;
                    if ($homegame->home_score > $homegame->away_score) {
                        $teams_profiles[$team->id]['wins']++;
                        $teams_profiles[$team->id]['point'] += 3;
                    } elseif ($homegame->home_score == $homegame->away_score) {
                        $teams_profiles[$team->id]['point'] += 1;
                        $teams_profiles[$team->id]['draws']++;
                    } else {
                        $teams_profiles[$team->id]['loose']++;
                    }
                }
                foreach ($team->awaygames as $awayGames) {
                    if ($awayGames->play_off == 1) {
                        continue;
                    }
                    if ($awayGames->home_score == null && $awayGames->away_score == null) {
                        continue;
                    }

                    $team_games[$team->id][] = $awayGames;
                    if ($awayGames->home_score < $awayGames->away_score) {
                        $teams_profiles[$team->id]['wins']++;
                        $teams_profiles[$team->id]['point'] += 3;
                    } elseif ($awayGames->home_score == $awayGames->away_score) {
                        $teams_profiles[$team->id]['point'] += 1;
                        $teams_profiles[$team->id]['draws']++;
                    } else {
                        $teams_profiles[$team->id]['loose']++;
                    }

                    $teams_profiles[$team->id]['goals_out'] += $awayGames->away_score;
                    $teams_profiles[$team->id]['goals_in'] += $awayGames->home_score;
                }
                $teams_profiles[$team->id]['goals'] = $teams_profiles[$team->id]['goals_out'] - $teams_profiles[$team->id]['goals_in'];
            }
            foreach ($team_games as $key => $value) {
                $tmp = collect($value)->sortBy('play_date')->reverse()->take(5);
                $teams_profiles[$key]['last_games'] = $tmp;
            }
            if ($group->id == 4) {
            }
            //'goals_out')->sortBy('goals')->sortBy('point'
           $ss = $group->teams->groupBy('points')->map(function($groupItems){
                    return $groupItems->sortByDesc('goals');
                });
            $team_prof_sorted = collect($group->teams)->groupBy('points')->map(function($groupItems){
                    return $groupItems->sortByDesc('goals');
            });
            foreach ($team_prof_sorted as $key => $value) {
                $team_prof_sorted[$key]['info'] = $teams_profiles[$team->id];
            }
            $tournament[$group->id]['profiles'] = $team_prof_sorted;
        }
        return (object)$tournament;
    }
}
