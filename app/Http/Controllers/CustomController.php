<?php

namespace App\Http\Controllers;

use App\Match;
use App\Highlight;
use App\Statistic;
use App\HighlightType;
use Illuminate\Http\Request;

class CustomController extends Controller
{

    public function manageGame($id)
    {
        $match = Match::with('homeId', 'awayId')->where('id', $id)->first();
        $statisticc = Statistic::with('player', 'match')->where('match_id', $id)->get();
        $statistic = [];
        foreach ($statisticc as $key => $value) {
            $statistic[$value->match_id][$value->player_id] = $value;
        }
        $homePlayer = $match->homeId()->first()->players()->get();
        $awayPlayer = $match->awayId()->first()->players()->get();
        $players = $homePlayer->merge($awayPlayer);

        $homeTeam = $match->homeId()->get();
        $awayTeam = $match->awayId()->get();
        $teams = $homeTeam->merge($awayTeam);

        $types = HighlightType::all();
        $highlights = Highlight::with('player', 'team')->where('match_id', $match->id)->orderBy('time', 'ASC')->get();
        return view('vendor.voyager.matches.manage', compact('match', 'statistic', 'highlights', 'players', 'types', 'teams'));
    }

    public function postGame(Request $request)
    {
        if ($request->get('action') == 'Statistic') {
            $statistic = $request->get('statistic');
            foreach ($statistic as $key => $value) {
                $match_id = $key;
                foreach ($value as $k => $v) {
                    $cs = Statistic::firstOrNew(['player_id' => $k, 'match_id' => $key]);
                    $cs->value = $v;
                    $cs->save();
                }
            }
            return redirect()->route('voyager.matches.manage', [$match_id]);
        } elseif ($request->get('action') == 'highlights') {
            $team_id = $request->get('team_id');
            $type_id = $request->get('type_id');
            $player_id = $request->get('player_id');
            $time = $request->get('time');
            $title = $request->get('title');
            $match_id = $request->get('match_id');
            Highlight::create(['team_id' => $team_id, 'type_id' => $type_id, 'player_id' => $player_id, 'time' => $time, 'title' => $title, 'match_id' => $match_id]);

            return redirect()->route('voyager.matches.manage', [$match_id]);
        } elseif ($request->get('action') == 'Score') {
            $homeScore = $request->get('home_score');
            $away_score = $request->get('away_score');
            $match_id = $request->get('matcher');
            $match = Match::find($match_id);
            $match->home_score = $homeScore;
            $match->away_score = $away_score;
            $match->save();
            return redirect()->route('voyager.matches.manage', [$match_id]);
        }
    }
}
