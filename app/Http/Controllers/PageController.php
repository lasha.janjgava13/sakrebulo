<?php

namespace App\Http\Controllers;

use Auth;
use MetaTag;
use App\Poll;
use App\Match;
use App\Player;
use App\Slider;
use App\Gallery;
use App\Partner;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class PageController extends Controller
{
    public function home(Request $request)
    {
        $slides = Slider::withCacheCooldownSeconds()->where('active', 1)->orderBy('priority', 'desc')->get();
        $mainNews = Post::where('status', 'PUBLISHED')->where('mainNews', 1)->with('category')->limit(3)->latest()->get();
        $freeNews = Post::where('status', 'PUBLISHED')->where('mainNews', 2)->latest()->first();
        $topPlayers = StatisticsController::getTopPlayers(4);
        $tournament = TournamentController::getMatches();
        $polls = PollStaticController::getActivePolls($request);
        return view('pages.home', compact('slides', 'mainNews', 'topPlayers', 'tournament', 'polls', 'freeNews'));
    }

    public function statistics()
    {
        $pageTitle = trans('website.statistics');
        MetaTag::set('title', $pageTitle);
        MetaTag::set('image', asset('images/logo.png'));
        $stats = StatisticsController::getTopPlayers(20);
        return view('pages.statistics', compact('stats', 'pageTitle'));
    }

    public function calendar()
    {
        $pageTitle = trans('website.calendar');

        return view('pages.calendar', compact('pageTitle'));
    }

    public function match($id)
    {
        $match = match::with('highlight.type', 'homeId.players.position', 'awayId.players.position', 'gallery')->findOrFail($id);
        $players = [];

        $match->homeId->players->merge($match->awayId->players)->each(function ($player) use (&$players) {
            $players[$player->id] = $player;
        });
#dd($match->awayID->players);
        $homeHighlights = $this->mapHighlights($match->highlight->where('team_id', $match->home_id)->groupBy('player_id'));
        $awayHighlights = $this->mapHighlights($match->highlight->where('team_id', $match->away_id)->groupBy('player_id'));
        #$match->videoId = $this->getVideoId($match->video);
        $gallery = $match->gallery ? json_decode($match->gallery->images) : [];

        MetaTag::set('title', $match->homeId->name . " VS " . $match->awayId->name);
        // MetaTag::set('image', asset('storage/' . $gallery[5]));

        return view('pages.match', compact('match', 'players', 'homeHighlights', 'awayHighlights', 'gallery'));
    }

    protected function getVideoId($url)
    {
        if (!$url) {
            return null;
        }
        $parts = parse_url($url);
	$query=[];
	
        parse_str($parts['query'],$query);
        return $query['path'];
    }

    protected function mapHighlights($highlights)
    {
        $teamHighlights = [];
        foreach ($highlights as $key => $playerHighlights) {
            $player = [
                'player_id' => $key,
                'highlights' => [],
            ];
            foreach ($playerHighlights as $highlight) {
                if ($highlight->type->display) {
                    if (!array_key_exists($highlight->type->id, $player['highlights'])) {
                        $player['highlights'][$highlight->type->id] = [
                            'count' => 0,
                            'item' => $highlight,
                        ];
                    }
                    $player['highlights'][$highlight->type->id]['count']++;
                }
            }
            if (count($player['highlights']) > 0) {
                $teamHighlights[] = $player;
            }
        }
        return $teamHighlights;
    }

    public function player($id)
    {
        $player = Player::with('team', 'position', 'highlights', 'home_games.awayId', 'away_games.homeId')->findOrFail($id);
        $locale = app()->getLocale();
        $pageTitle = $player->fullName;
        $highlightIds = [
            'goal' => 2,
            'assist' => 3,
            'yellow_card' => 4,
            'red_card' => 5,
        ];
        $highlights = (object)[
            'red_card' => 0,
            'yellow_card' => 0,
            'goal' => 0,
            'assist' => 0,
        ];
        $matches = $player->home_games->map(function (&$item) {
            $item->isHome = true;
            return $item;
        })->merge($player->away_games)->sortByDesc('play_date');
        foreach ($player->highlights as $highlight) {
            switch ($highlight->type_id) {
                case $highlightIds['goal']:
                    $highlights->goal++;
                    break;
                case $highlightIds['assist']:
                    $highlights->assist++;
                    break;
                case $highlightIds['red_card']:
                    $highlights->red_card++;
                    break;
                case $highlightIds['yellow_card']:
                    $highlights->yellow_card++;
                    break;
            }
        }
        $polles = $player->polles()->get();
        foreach ($polles as $key => $value) {
            $value->pollMax('players', $player);
        }

        // return $matches;
        MetaTag::set('title', $pageTitle);
        MetaTag::set('image', asset('storage/' . $player->image));
        return view('pages.player', compact('pageTitle', 'player', 'highlights', 'matches'));
    }

    public function standings()
    {
        $pageTitle = trans('website.standings');
        $tournament = TournamentController::getMatches();
        // return response()->json($tournament);
        return view('pages.standings', compact('pageTitle', 'tournament'));
    }
     public function standingsDev()
    {
        $pageTitle = trans('website.standings');
        $tournament = TournamentController::getMatchesDev();
        // return response()->json($tournament);
        return view('pages.standings2', compact('pageTitle', 'tournament'));
    }

    /**
     * Display gallery listing
     *
     * @return \Illuminate\View\View
     */
    public function gallery()
    {
        $galleries = Gallery::with('translations')->latest()->paginate(15);
        return view('pages.gallery', compact('galleries'));
    }
}
