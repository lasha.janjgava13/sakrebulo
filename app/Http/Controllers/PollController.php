<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\PollAnswer;
use App\User;
use Carbon\Carbon;
use Cookie;
use TCG\Voyager\Models\Setting;

class PollController extends Controller
{
    public function index()
    {
        $polls = Poll::all();
        $return_array = [];
        foreach ($polls as $poll) {
            foreach ($poll->answers as $answer) {
                $return_array[$poll->id][$answer->id]++;
            }
        }
    }

    public function save(Request $request)
    {
        $ip = request()->ip();
        $poll_id = $request->get('poll_id');
        // $user_id = $request->get('user_id');
        // if($user_id == null || $user_id == 0){
        //     return response()->json(['status' => 0, 'message' => 'Cant Add New']);
        // }
        // $user = User::find($user_id);
        // if(!$user){
        //     return response()->json(['status' => 0, 'message' => 'Cant Add New']);
        // }
        if ($request->hasCookie('poll_' . $poll_id)) {
            dd($request->all());
            return response()->json(['status' => 0, 'message' => 'Cant Add New']);
        }
        $cookie = cookie('poll_' . $poll_id, '1', 1440);
        $answer_id = $request->get('answer_id');
        $time = Carbon::now()->toDateTimeString();
        // $poll = PollAnswer::where('poll_id', $poll_id)->where('user_id',$user_id)->orderBy('polled','DESC')->first();
        $poll = PollAnswer::where('poll_id', $poll_id)->where('ip',$ip)->orderBy('polled','DESC')->first();
        $time = Carbon::now()->toDateTimeString();
        if (!$poll) {
            // Poll::find($poll_id)->answers()->save(new PollAnswer(['ip' => $ip, 'answer_id' => $answer_id, 'user_id'=>$user_id, 'polled' => Carbon::parse($time)->timestamp]));
            Poll::find($poll_id)->answers()->save(new PollAnswer(['ip' => $ip, 'answer_id' => $answer_id,  'polled' => Carbon::parse($time)->timestamp]));
        } else {
            if (Carbon::parse($poll->polled)->diff(Carbon::parse($time))->format('%d') >= 1) {
                Poll::find($poll_id)->answers()->save(new PollAnswer(['ip' => $ip, 'answer_id' => $answer_id, 'polled' => Carbon::parse($time)->timestamp]));
            } else {
                dd(['check poll->polled status',Carbon::parse($poll->polled),Carbon::parse($time)]);
                return response()->json(['status' => 0, 'message' => 'Cant Add New']);
            }
        }
        return response()->json(['status' => 1, 'message' => 'Added']);

    }

    public function activate($id)
    {
        $poll = Poll::findOrFail($id);
        $type = $poll->type_id === 1 ? 'teams' : 'players';
        $key = 'site.' . $type . '_poll_id';
        $old = setting($key);
        Setting::where('key', $key)->update(['value' => $id]);
        return response()->json([
            'old' => $old
        ]);
    }
}
