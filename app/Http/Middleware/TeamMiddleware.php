<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class TeamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $tournament = config('app.tournament');
        // $teams = Team::with('region')->where('active', 1)->get();
        $date = Carbon::now();
        if (config('app.tournament')) {
            $tournament = config('app.tournament');
        } else {
            $tournament = Tournament::whereRaw(
                "end_date >= ? AND start_date <= ?",
                array($date, $date)
            )->first();
        }
        $teams = $tournament->groups->map(function ($group) {
            return $group->teams->where('active', 1);
        })->flatten();
        View::share('teams', $teams);
        config(['app.teams' => $teams]);
        return $next($request);
    }
}
