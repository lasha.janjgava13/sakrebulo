<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Tournament;

class SubdomainMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $server = explode('.', $request->server('HTTP_HOST'));
        $subdomain = !in_array($server[0], [config('app.domain'), 'www']) ? $server[0] : false;

        config(['app.subdomain' => $subdomain]);
        if(env('APP_ENV') == 'development'){
            $tournament = Tournament::where('active', 1)->with('groups.teams')->orderBy('start_date', 'desc')->first();
        }
        elseif ($subdomain === false) {
            $tournament = Tournament::where('active', 1)->with('groups.teams')->orderBy('start_date', 'desc')->first();
        } else {
            $tournament = Tournament::where('slug', $subdomain)->with('groups.teams')->first();
            if (is_null($tournament)) {
                abort(404);
            }
        }
        config(['app.tournament' => $tournament]);
        return $next($request);
    }
}
