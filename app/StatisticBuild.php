<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatisticBuild extends Model
{
    protected $table = 'StatisticBuild';
    public function player(){
        return $this->belongsTo(Player::class);
    }
}
