<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Tournament extends Model
{
    #use Cachable;

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
