<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class School extends Model
{
    use Translatable;

    protected $translatable = ['title'];

    public function players()
    {
        return $this->hasMany(Player::class);
    }
}
