<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class PollAnswer extends Model
{

    #use Cachable;

    protected $cacheCooldownSeconds = 600;

    protected $fillable = ['ip', 'answer_id', 'polled','user_id'];
	
	protected $dates = ['created_at', 'updated_at', 'polled'];

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }
}
