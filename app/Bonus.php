<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Bonus extends Model
{
	public function getTypeVal()
	{
		return $this->type()->first()->point;
	}
    public function type()
    {
    	return $this->belongsTo(BonusType::class);
    }
}
