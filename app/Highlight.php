<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Highlight extends Model
{

    use Translatable; # Cachable;

    protected $fillable = ['title', 'player_id', 'type_id', 'time', 'team_id', 'match_id'];

    protected $translatable = ['title'];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function type()
    {
        return $this->belongsTo(HighlightType::class);
    }
}
