<?php

namespace App;

use TCG\Voyager\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use Translatable;

    protected $translatable = ['title'];

    public function getImageUrlAttribute()
    {
        if (!is_null($this->image)) {
            return Voyager::image($this->image);
        }
        $parts = parse_url($this->url);
        parse_str($parts['query'], $videoId);
        return "http://img.youtube.com/vi/{$videoId['v']}/maxresdefault.jpg";
    }
}
