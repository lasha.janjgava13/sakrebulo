<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Partner extends Model
{
    use Cachable;

    // protected $cacheCooldownSeconds = 3600;
    protected $cacheCooldownSeconds = 0;
}
