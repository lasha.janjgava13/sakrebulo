<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Team extends Model
{
    use \Awobaz\Compoships\Compoships,
        Translatable;

    protected $translatable = ['name'];


    public function players()
    {
        return $this->hasMany(Player::class);
    }

    public function games()
    {
        return $this->hasMany(Match::class, ['home_id', 'id'], ['away_id', 'id']);
    }

    public function homegames()
    {
        return $this->hasMany(Match::class, 'home_id', 'id');
    }

    public function awaygames()
    {
        return $this->hasMany(Match::class, 'away_id', 'id');
    }


    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function goals(){
        $goals = 0;
        $goals_in = 0;
        $goals_out = 0;
        $homeGamesCol = $this->homegames()->get();
        $awayGamesCol = $this->awaygames()->get();
        foreach ($awayGamesCol as $awayGames) {
            if ($awayGames->play_off == 1) {
                continue;
            }
            if ($awayGames->home_score == null && $awayGames->away_score == null) {
                continue;
            }
            $goals_in += $awayGames->away_score;
            $goals_out += $awayGames->home_score;
           
        }
         foreach ($homeGamesCol as $homeGame) {
            if ($homeGame->play_off == 1) {
                continue;
            }
            if ($homeGame->home_score == null && $homeGame->away_score == null) {
                continue;
            }
           $goals_in += $homeGame->home_score;
           $goals_out += $homeGame->away_score;
        }
        $this->attributes['goals'] =  $goals_in-$goals_out;
    }



    public function points(){
        $point = 0;
        $homeGamesCol = $this->homegames()->get();
        $awayGamesCol = $this->awaygames()->get();
       foreach ($awayGamesCol as $awayGames) {
            if ($awayGames->play_off == 1) {
                continue;
            }
            if ($awayGames->home_score == null && $awayGames->away_score == null) {
                continue;
            }
            if ($awayGames->home_score < $awayGames->away_score) {
                $point += 3;
            } elseif ($awayGames->home_score == $awayGames->away_score) {
                $point += 1;
            } 
        }
         foreach ($homeGamesCol as $homeGame) {
            if ($homeGame->play_off == 1) {
                continue;
            }
            if ($homeGame->home_score == null && $homeGame->away_score == null) {
                continue;
            }
            if ($homeGame->home_score > $homeGame->away_score) {
                $point += 3;
            } elseif ($homeGame->home_score == $homeGame->away_score) {
                $point += 1;
            } 
        }
        $this->attributes['points'] =  $point;
    }
}
