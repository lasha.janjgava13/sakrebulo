<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Player extends Model
{
    use Translatable;
        #Cachable;

    protected $translatable = ['first_name', 'last_name', 'text'];

    /**
     * Additional atributes for Voyager
     * @var array
     */
    public $additional_attributes = ['full_name'];

    /**
     * Mutator for full_name attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $locale = app()->getLocale();
        $firstName = $this->getTranslatedAttribute('first_name', $locale);
        $lastName = $this->getTranslatedAttribute('last_name', $locale);
        return $firstName . ' ' . $lastName;
    }
    public function polles()
    {
        return $this->belongsToMany(Poll::class, 'poll_questions', 'player_id', 'poll_id')->withPivot('team_id');
    }
    public function Team()
    {
        return $this->BelongsTo(Team::class);
    }

    public function teamId()
    {
        return $this->BelongsTo(Team::class);
    }

    public function highlights()
    {
        return $this->HasMany(Highlight::class);
    }

    public function statistic()
    {
        return $this->hasMany(Statistic::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * Get Player Rating based on highlights
     *
     * @return int
     */
    public function getRating()
    {
        $stats = $this->statistic()->where('value','!=','0')->get();
        $sum = $stats->sum('value');
        $count = $stats->count();
        return ($count != 0) ? $sum / $count : 0;
    }
    public function getBonus()
    {
        $highlights = $this->highlights()->get();
        $count = 0;
        foreach ($highlights as $key => $value) {
            switch ($value->type_id) {
                case '2':
                    $count += 1;
                    break;
                case '3':
                    $count += 0.75;
                    break;
                case '4':
                    $count -= 1;
                    break;
                case '5':
                    $count -= 3;
                    break;
                case '6':
                    $count += 3;
                    break;
            }
        }
        $polles = $this->polles()->get();
        foreach ($polles as $key => $value) {
           if( $value->pollMax('players',$this)){
                 $count += 10;
           }
        }
        $bonus = $this->Bonuses()->get();
        foreach ($bonus as $key => $value) {
            $val = $value->getTypeVal();
            if($val){ $count += $val;}
        }
        return $count;
    }
    /**
     * Get Player Rating based on Game
     *
     * @return int
     */
    public function getRatingByGame($game)
    {
        $current = $this->statistic()->where('match_id',$game->id)->first();
        return $current ? $current->value : '';
    }
    public function getPlayerGamesCount()
    {
        $current = $this->statistic()->where('value','!=','0')->count();
        return $current ? $current : 0;
    }
    public function home_games()
    {
        return $this->belongsToMany(Match::class, 'match_players_home');
    }

    public function away_games()
    {
        return $this->belongsToMany(Match::class, 'match_players_away');
    }

    public function Bonuses()
    {
        return $this->hasMany(Bonus::class);
    }
}
