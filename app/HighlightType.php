<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class HighlightType extends Model
{
    use Translatable; # Cachable;

    protected $translatable = ['title'];
}
