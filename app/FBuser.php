<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FBuser extends Model
{

	protected $table = 'fbuser';
    protected $fillable = [

         'name', 'email', 'password', 'provider', 'provider_id'

     ];
}
