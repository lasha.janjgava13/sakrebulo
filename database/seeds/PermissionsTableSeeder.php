<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2019-03-03 01:33:38',
                'updated_at' => '2019-03-03 01:33:38',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'browse_teams',
                'table_name' => 'teams',
                'created_at' => '2019-03-03 01:43:31',
                'updated_at' => '2019-03-03 01:43:31',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'read_teams',
                'table_name' => 'teams',
                'created_at' => '2019-03-03 01:43:31',
                'updated_at' => '2019-03-03 01:43:31',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'edit_teams',
                'table_name' => 'teams',
                'created_at' => '2019-03-03 01:43:31',
                'updated_at' => '2019-03-03 01:43:31',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'add_teams',
                'table_name' => 'teams',
                'created_at' => '2019-03-03 01:43:31',
                'updated_at' => '2019-03-03 01:43:31',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'delete_teams',
                'table_name' => 'teams',
                'created_at' => '2019-03-03 01:43:31',
                'updated_at' => '2019-03-03 01:43:31',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'browse_players',
                'table_name' => 'players',
                'created_at' => '2019-03-03 01:57:25',
                'updated_at' => '2019-03-03 01:57:25',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'read_players',
                'table_name' => 'players',
                'created_at' => '2019-03-03 01:57:25',
                'updated_at' => '2019-03-03 01:57:25',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'edit_players',
                'table_name' => 'players',
                'created_at' => '2019-03-03 01:57:25',
                'updated_at' => '2019-03-03 01:57:25',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'add_players',
                'table_name' => 'players',
                'created_at' => '2019-03-03 01:57:25',
                'updated_at' => '2019-03-03 01:57:25',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'delete_players',
                'table_name' => 'players',
                'created_at' => '2019-03-03 01:57:25',
                'updated_at' => '2019-03-03 01:57:25',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'browse_matches',
                'table_name' => 'matches',
                'created_at' => '2019-03-03 02:02:22',
                'updated_at' => '2019-03-03 02:02:22',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'read_matches',
                'table_name' => 'matches',
                'created_at' => '2019-03-03 02:02:22',
                'updated_at' => '2019-03-03 02:02:22',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'edit_matches',
                'table_name' => 'matches',
                'created_at' => '2019-03-03 02:02:22',
                'updated_at' => '2019-03-03 02:02:22',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'add_matches',
                'table_name' => 'matches',
                'created_at' => '2019-03-03 02:02:22',
                'updated_at' => '2019-03-03 02:02:22',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'delete_matches',
                'table_name' => 'matches',
                'created_at' => '2019-03-03 02:02:22',
                'updated_at' => '2019-03-03 02:02:22',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'browse_tournaments',
                'table_name' => 'tournaments',
                'created_at' => '2019-04-14 19:52:11',
                'updated_at' => '2019-04-14 19:52:11',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'read_tournaments',
                'table_name' => 'tournaments',
                'created_at' => '2019-04-14 19:52:11',
                'updated_at' => '2019-04-14 19:52:11',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'edit_tournaments',
                'table_name' => 'tournaments',
                'created_at' => '2019-04-14 19:52:11',
                'updated_at' => '2019-04-14 19:52:11',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'add_tournaments',
                'table_name' => 'tournaments',
                'created_at' => '2019-04-14 19:52:11',
                'updated_at' => '2019-04-14 19:52:11',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'delete_tournaments',
                'table_name' => 'tournaments',
                'created_at' => '2019-04-14 19:52:11',
                'updated_at' => '2019-04-14 19:52:11',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'browse_matches',
                'table_name' => 'matches',
                'created_at' => '2019-04-14 20:06:14',
                'updated_at' => '2019-04-14 20:06:14',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'read_matches',
                'table_name' => 'matches',
                'created_at' => '2019-04-14 20:06:14',
                'updated_at' => '2019-04-14 20:06:14',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'edit_matches',
                'table_name' => 'matches',
                'created_at' => '2019-04-14 20:06:14',
                'updated_at' => '2019-04-14 20:06:14',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'add_matches',
                'table_name' => 'matches',
                'created_at' => '2019-04-14 20:06:14',
                'updated_at' => '2019-04-14 20:06:14',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'delete_matches',
                'table_name' => 'matches',
                'created_at' => '2019-04-14 20:06:14',
                'updated_at' => '2019-04-14 20:06:14',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'browse_groups',
                'table_name' => 'groups',
                'created_at' => '2019-04-14 20:17:35',
                'updated_at' => '2019-04-14 20:17:35',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'read_groups',
                'table_name' => 'groups',
                'created_at' => '2019-04-14 20:17:35',
                'updated_at' => '2019-04-14 20:17:35',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'edit_groups',
                'table_name' => 'groups',
                'created_at' => '2019-04-14 20:17:35',
                'updated_at' => '2019-04-14 20:17:35',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'add_groups',
                'table_name' => 'groups',
                'created_at' => '2019-04-14 20:17:35',
                'updated_at' => '2019-04-14 20:17:35',
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'delete_groups',
                'table_name' => 'groups',
                'created_at' => '2019-04-14 20:17:35',
                'updated_at' => '2019-04-14 20:17:35',
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'browse_highlights',
                'table_name' => 'highlights',
                'created_at' => '2019-04-14 20:36:08',
                'updated_at' => '2019-04-14 20:36:08',
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'read_highlights',
                'table_name' => 'highlights',
                'created_at' => '2019-04-14 20:36:08',
                'updated_at' => '2019-04-14 20:36:08',
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'edit_highlights',
                'table_name' => 'highlights',
                'created_at' => '2019-04-14 20:36:08',
                'updated_at' => '2019-04-14 20:36:08',
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'add_highlights',
                'table_name' => 'highlights',
                'created_at' => '2019-04-14 20:36:08',
                'updated_at' => '2019-04-14 20:36:08',
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'delete_highlights',
                'table_name' => 'highlights',
                'created_at' => '2019-04-14 20:36:08',
                'updated_at' => '2019-04-14 20:36:08',
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'browse_highlight_types',
                'table_name' => 'highlight_types',
                'created_at' => '2019-04-14 20:44:06',
                'updated_at' => '2019-04-14 20:44:06',
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'read_highlight_types',
                'table_name' => 'highlight_types',
                'created_at' => '2019-04-14 20:44:06',
                'updated_at' => '2019-04-14 20:44:06',
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'edit_highlight_types',
                'table_name' => 'highlight_types',
                'created_at' => '2019-04-14 20:44:06',
                'updated_at' => '2019-04-14 20:44:06',
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'add_highlight_types',
                'table_name' => 'highlight_types',
                'created_at' => '2019-04-14 20:44:06',
                'updated_at' => '2019-04-14 20:44:06',
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'delete_highlight_types',
                'table_name' => 'highlight_types',
                'created_at' => '2019-04-14 20:44:06',
                'updated_at' => '2019-04-14 20:44:06',
            ),
            81 => 
            array (
                'id' => 82,
                'key' => 'browse_partners',
                'table_name' => 'partners',
                'created_at' => '2019-05-06 21:50:33',
                'updated_at' => '2019-05-06 21:50:33',
            ),
            82 => 
            array (
                'id' => 83,
                'key' => 'read_partners',
                'table_name' => 'partners',
                'created_at' => '2019-05-06 21:50:33',
                'updated_at' => '2019-05-06 21:50:33',
            ),
            83 => 
            array (
                'id' => 84,
                'key' => 'edit_partners',
                'table_name' => 'partners',
                'created_at' => '2019-05-06 21:50:33',
                'updated_at' => '2019-05-06 21:50:33',
            ),
            84 => 
            array (
                'id' => 85,
                'key' => 'add_partners',
                'table_name' => 'partners',
                'created_at' => '2019-05-06 21:50:33',
                'updated_at' => '2019-05-06 21:50:33',
            ),
            85 => 
            array (
                'id' => 86,
                'key' => 'delete_partners',
                'table_name' => 'partners',
                'created_at' => '2019-05-06 21:50:33',
                'updated_at' => '2019-05-06 21:50:33',
            ),
            86 => 
            array (
                'id' => 87,
                'key' => 'browse_sliders',
                'table_name' => 'sliders',
                'created_at' => '2019-05-09 18:02:35',
                'updated_at' => '2019-05-09 18:02:35',
            ),
            87 => 
            array (
                'id' => 88,
                'key' => 'read_sliders',
                'table_name' => 'sliders',
                'created_at' => '2019-05-09 18:02:35',
                'updated_at' => '2019-05-09 18:02:35',
            ),
            88 => 
            array (
                'id' => 89,
                'key' => 'edit_sliders',
                'table_name' => 'sliders',
                'created_at' => '2019-05-09 18:02:35',
                'updated_at' => '2019-05-09 18:02:35',
            ),
            89 => 
            array (
                'id' => 90,
                'key' => 'add_sliders',
                'table_name' => 'sliders',
                'created_at' => '2019-05-09 18:02:35',
                'updated_at' => '2019-05-09 18:02:35',
            ),
            90 => 
            array (
                'id' => 91,
                'key' => 'delete_sliders',
                'table_name' => 'sliders',
                'created_at' => '2019-05-09 18:02:35',
                'updated_at' => '2019-05-09 18:02:35',
            ),
            91 => 
            array (
                'id' => 92,
                'key' => 'browse_tops',
                'table_name' => 'tops',
                'created_at' => '2019-05-13 23:33:22',
                'updated_at' => '2019-05-13 23:33:22',
            ),
            92 => 
            array (
                'id' => 93,
                'key' => 'read_tops',
                'table_name' => 'tops',
                'created_at' => '2019-05-13 23:33:22',
                'updated_at' => '2019-05-13 23:33:22',
            ),
            93 => 
            array (
                'id' => 94,
                'key' => 'edit_tops',
                'table_name' => 'tops',
                'created_at' => '2019-05-13 23:33:22',
                'updated_at' => '2019-05-13 23:33:22',
            ),
            94 => 
            array (
                'id' => 95,
                'key' => 'add_tops',
                'table_name' => 'tops',
                'created_at' => '2019-05-13 23:33:22',
                'updated_at' => '2019-05-13 23:33:22',
            ),
            95 => 
            array (
                'id' => 96,
                'key' => 'delete_tops',
                'table_name' => 'tops',
                'created_at' => '2019-05-13 23:33:22',
                'updated_at' => '2019-05-13 23:33:22',
            ),
            96 => 
            array (
                'id' => 97,
                'key' => 'browse_toptypes',
                'table_name' => 'toptypes',
                'created_at' => '2019-05-13 23:38:30',
                'updated_at' => '2019-05-13 23:38:30',
            ),
            97 => 
            array (
                'id' => 98,
                'key' => 'read_toptypes',
                'table_name' => 'toptypes',
                'created_at' => '2019-05-13 23:38:30',
                'updated_at' => '2019-05-13 23:38:30',
            ),
            98 => 
            array (
                'id' => 99,
                'key' => 'edit_toptypes',
                'table_name' => 'toptypes',
                'created_at' => '2019-05-13 23:38:30',
                'updated_at' => '2019-05-13 23:38:30',
            ),
            99 => 
            array (
                'id' => 100,
                'key' => 'add_toptypes',
                'table_name' => 'toptypes',
                'created_at' => '2019-05-13 23:38:30',
                'updated_at' => '2019-05-13 23:38:30',
            ),
            100 => 
            array (
                'id' => 101,
                'key' => 'delete_toptypes',
                'table_name' => 'toptypes',
                'created_at' => '2019-05-13 23:38:30',
                'updated_at' => '2019-05-13 23:38:30',
            ),
            101 => 
            array (
                'id' => 107,
                'key' => 'browse_schools',
                'table_name' => 'schools',
                'created_at' => '2019-05-24 02:32:14',
                'updated_at' => '2019-05-24 02:32:14',
            ),
            102 => 
            array (
                'id' => 108,
                'key' => 'read_schools',
                'table_name' => 'schools',
                'created_at' => '2019-05-24 02:32:14',
                'updated_at' => '2019-05-24 02:32:14',
            ),
            103 => 
            array (
                'id' => 109,
                'key' => 'edit_schools',
                'table_name' => 'schools',
                'created_at' => '2019-05-24 02:32:14',
                'updated_at' => '2019-05-24 02:32:14',
            ),
            104 => 
            array (
                'id' => 110,
                'key' => 'add_schools',
                'table_name' => 'schools',
                'created_at' => '2019-05-24 02:32:14',
                'updated_at' => '2019-05-24 02:32:14',
            ),
            105 => 
            array (
                'id' => 111,
                'key' => 'delete_schools',
                'table_name' => 'schools',
                'created_at' => '2019-05-24 02:32:14',
                'updated_at' => '2019-05-24 02:32:14',
            ),
            106 => 
            array (
                'id' => 112,
                'key' => 'browse_positions',
                'table_name' => 'positions',
                'created_at' => '2019-05-24 02:32:48',
                'updated_at' => '2019-05-24 02:32:48',
            ),
            107 => 
            array (
                'id' => 113,
                'key' => 'read_positions',
                'table_name' => 'positions',
                'created_at' => '2019-05-24 02:32:48',
                'updated_at' => '2019-05-24 02:32:48',
            ),
            108 => 
            array (
                'id' => 114,
                'key' => 'edit_positions',
                'table_name' => 'positions',
                'created_at' => '2019-05-24 02:32:48',
                'updated_at' => '2019-05-24 02:32:48',
            ),
            109 => 
            array (
                'id' => 115,
                'key' => 'add_positions',
                'table_name' => 'positions',
                'created_at' => '2019-05-24 02:32:48',
                'updated_at' => '2019-05-24 02:32:48',
            ),
            110 => 
            array (
                'id' => 116,
                'key' => 'delete_positions',
                'table_name' => 'positions',
                'created_at' => '2019-05-24 02:32:48',
                'updated_at' => '2019-05-24 02:32:48',
            ),
        ));
        
        
    }
}