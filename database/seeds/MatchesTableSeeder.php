<?php

use Illuminate\Database\Seeder;

class MatchesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('matches')->delete();
        
        \DB::table('matches')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'name for game',
                'play_date' => '2019-03-13 21:00:00',
                'home_id' => 2,
                'away_id' => 1,
                'home_score' => 3,
                'away_score' => 2,
                'review' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-05-21 03:10:14',
            ),
        ));
        
        
    }
}