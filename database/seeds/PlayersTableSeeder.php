<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('players')->delete();
        
        \DB::table('players')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'lasha',
                'last_name' => 'janjgava',
                'image' => NULL,
                'team_id' => 2,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 25,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'tengo',
                'last_name' => 'nawyebia',
                'image' => NULL,
                'team_id' => 2,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 24,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'first_name' => 'vaxo',
                'last_name' => 'chachava',
                'image' => NULL,
                'team_id' => 1,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 20,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'first_name' => 'giorgi',
                'last_name' => 'test',
                'image' => NULL,
                'team_id' => 1,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 19,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'first_name' => 'leva',
                'last_name' => 'jojua',
                'image' => NULL,
                'team_id' => 1,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 18,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'first_name' => 'jumber',
                'last_name' => 'lejava',
                'image' => NULL,
                'team_id' => 2,
                'birth_date' => '2019-05-08',
                'height' => 150,
                'weight' => 85,
                'number' => 9,
                'active' => 1,
                'type' => 'player',
                'created_at' => '2019-05-09 15:07:54',
                'updated_at' => '2019-05-09 15:22:42',
                'school_id' => NULL,
                'position_id' => NULL,
            ),
        ));
        
        
    }
}