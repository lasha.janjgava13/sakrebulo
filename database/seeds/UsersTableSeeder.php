<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$XQ4M1TRYilbPBeLRkngHKeK/rg/fVxqIT4CbVCSH0nfgLUSNJjlF.',
                'remember_token' => 'dAKEnlv7cFDTrtCSiD6AHqp4UDVxvgcxbLPgaAEEXqYjbA9ErJXOarukyHHO',
                'settings' => NULL,
                'created_at' => '2019-03-03 01:33:39',
                'updated_at' => '2019-03-03 01:33:39',
            ),
        ));
        
        
    }
}