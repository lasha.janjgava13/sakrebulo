<?php

use Illuminate\Database\Seeder;

class HighlightTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('highlight_types')->delete();
        
        \DB::table('highlight_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'cc',
                'display' => 0,
                'created_at' => '2019-05-09 15:47:03',
                'updated_at' => '2019-05-21 03:16:07',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Gool',
                'display' => 1,
                'created_at' => '2019-05-21 03:11:04',
                'updated_at' => '2019-05-21 03:11:04',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Red Card',
                'display' => 1,
                'created_at' => '2019-05-21 03:11:29',
                'updated_at' => '2019-05-21 03:11:29',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Yellow Card',
                'display' => 1,
                'created_at' => '2019-05-21 03:11:51',
                'updated_at' => '2019-05-21 03:11:51',
            ),
        ));
        
        
    }
}