<?php

use Illuminate\Database\Seeder;

class HighlightsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('highlights')->delete();
        
        \DB::table('highlights')->insert(array (
            0 => 
            array (
                'id' => 4,
                'title' => NULL,
                'player_id' => 6,
                'type_id' => 4,
                'time' => 14,
                'created_at' => '2019-05-21 03:18:30',
                'updated_at' => '2019-05-21 03:18:30',
                'team_id' => 1,
                'match_id' => 1,
            ),
            1 => 
            array (
                'id' => 5,
                'title' => NULL,
                'player_id' => 3,
                'type_id' => 2,
                'time' => 20,
                'created_at' => '2019-05-21 03:21:48',
                'updated_at' => '2019-05-21 03:21:48',
                'team_id' => 1,
                'match_id' => 1,
            ),
            2 => 
            array (
                'id' => 6,
                'title' => NULL,
                'player_id' => 1,
                'type_id' => 3,
                'time' => 35,
                'created_at' => '2019-05-21 03:22:06',
                'updated_at' => '2019-05-21 03:22:06',
                'team_id' => 2,
                'match_id' => 1,
            ),
            3 => 
            array (
                'id' => 7,
                'title' => NULL,
                'player_id' => 5,
                'type_id' => 2,
                'time' => 65,
                'created_at' => '2019-05-21 03:24:24',
                'updated_at' => '2019-05-21 03:24:24',
                'team_id' => 2,
                'match_id' => 1,
            ),
        ));
        
        
    }
}