<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->text('image', 65535)->nullable();
			$table->integer('team_id')->unsigned()->nullable()->index();
			$table->date('birth_date')->nullable();
			$table->integer('height')->nullable();
			$table->integer('weight')->nullable();
			$table->integer('number')->default(1);
			$table->boolean('active')->default(1);
			$table->enum('type', array('manager','player'))->default('player');
			$table->timestamps();
			$table->integer('school_id')->nullable()->index();
			$table->integer('position_id')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
