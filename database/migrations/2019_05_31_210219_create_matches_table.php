<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matches', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->dateTime('play_date')->nullable();
			$table->integer('home_id')->unsigned()->nullable()->index('games_home_id_index');
			$table->integer('away_id')->unsigned()->nullable()->index('games_away_id_index');
			$table->integer('home_score')->unsigned()->default(0);
			$table->integer('away_score')->unsigned()->default(0);
			$table->text('review', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matches');
	}

}
