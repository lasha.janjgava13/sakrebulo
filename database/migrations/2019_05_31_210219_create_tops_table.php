<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team_id')->nullable()->index();
			$table->integer('player_id')->nullable()->index();
			$table->integer('type_id')->nullable()->index();
			$table->integer('score')->nullable();
			$table->timestamps();
			$table->integer('count')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tops');
	}

}
