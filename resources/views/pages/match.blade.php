@extends('layouts.app')

@php
    $matchDate = Carbon\Carbon::parse($match->play_date);
@endphp

@section('content')
<div class="match w-100">
    <div class="match__header">
        <div class="container h-100 d-flex justify-content-center align-items-end flex-wrap">
            @if(!is_null($match->getTranslatedAttribute('stadium', $locale)) && mb_strlen($match->getTranslatedAttribute('stadium', $locale)) > 3)
            <div class="d-flex justify-content-center align-items-center">
                <img class="match__stadium-icon mr-2" src="{{ url('/images/stadium.png') }}" srcset="{{ url('/images/stadium@2x.png') }} 2x, {{ url('/images/stadium@3x.png') }} 3x"/>
                <span class="text-excelsior-caps text-white fs-14">{{ $match->getTranslatedAttribute('stadium', $locale) }}</span>
            </div>
            @endif
            <div class="match__table w-100 col-12 d-flex justify-content-between align-items-center">
                <div class="match__table__block h-100 d-flex justify-content-start align-items-center">
                    <h4 class="text-excelsior-exp text-white fs-18 mb-0">{{ $match->homeId->getTranslatedAttribute('name', $locale) }}</h4>
                </div>
                <div class="h-100 match__table__block match__table__block--result">
                    <div class="match__score d-flex flex-center bg-white text-black h-100 px-2 fs-24">
                        @if(is_null($match->home_score) || is_null($match->home_score))
                        {{ $matchDate->format('H:i') }}
                        @else
                        {{ $match->home_score }} - {{ $match->away_score }}
                        @endif
                    </div>
                </div>
                <div class="match__table__block h-100 d-flex justify-content-end align-items-center text-right">
                    <h4 class="text-excelsior-exp text-white fs-18 mb-0">{{ $match->awayId->getTranslatedAttribute('name', $locale) }}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="match__info w-100 bg-darker-blue">
        <div class="container">
            <div class="row justify-content-center align-items-start">
                <div class="col-3 py-3">
                    @foreach($homeHighlights as $item)
                    <div class="w-100 text-right text-white mb-3 text-excelsior-caps">
                        <strong>{{ $players[$item['player_id']]->fullName }}</strong>
                        -
                        @foreach($item['highlights'] as $highlight)
                        @if($highlight['item']->type->display)
                            @if($highlight['count'] == 1)
                            <img class="match__highlight-icon mx-1" src="{{ Voyager::image($highlight['item']->type->icon) }}" alt="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" title="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" />
                            @else
                            <span class="mx-1 white-space:nowrap">
                                {{ $highlight['count'] }}x
                                <img class="match__highlight-icon" src="{{ Voyager::image($highlight['item']->type->icon) }}" alt="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" title="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" />
                            </span>
                            @endif
                        @endif
                        @endforeach
                    </div>
                    @endforeach
                </div>
                <div class="col-6 d-flex justify-content-center align-items-start">
                    <div class="match__info__block">
                        <img class="match__info__logo object-fit:contain" src="{{ Voyager::image($match->homeId->logo) }}" alt="{{ $match->homeId->getTranslatedAttribute('name', $locale) }}" title="{{ $match->homeId->getTranslatedAttribute('name', $locale) }}" />
                    </div>
                    <div class="match__info__block match__info__block--middle mx-4">
                        @if($match->video)
                        <a href="{{ $match->video }}" target="_blank" rel="noopener" class="d-inline-block text-center rounded-0 border-0 bg-light-red w-100 text-excelsior-caps text-white fs-14 py-2">{{ trans('website.watch') }} <i class="far fa-play-circle"></i></a>
                        @endif
                        <div class="d-block w-100 mt-3 text-center">
                            <span class="text-white text-excelsior-caps fs-14 white-space:nowrap">{{ $matchDate->format('j') }} {{ trans('calendar.' . strtolower($matchDate->format('F'))) }} {{ $matchDate->format('Y') }}</span>
                        </div>
                    </div>
                    <div class="match__info__block">
                        <img class="match__info__logo object-fit:contain" src="{{ Voyager::image($match->awayId->logo) }}" alt="{{ $match->awayId->getTranslatedAttribute('name', $locale) }}" title="{{ $match->awayId->getTranslatedAttribute('name', $locale) }}" />
                    </div>
                </div>
                <div class="col-3 py-3">
                   @foreach($awayHighlights as $item)
                    <div class="w-100 text-left text-white mb-3 text-excelsior-caps">
                        <strong>{{ $players[$item['player_id']]->fullName }}</strong>
                        -
                        @foreach($item['highlights'] as $highlight)
                        @if($highlight['item']->type->display)
                            @if($highlight['count'] == 1)
                            <img class="match__highlight-icon mx-1" src="{{ Voyager::image($highlight['item']->type->icon) }}" alt="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" title="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" />
                            @else
                            <span class="mx-1 white-space:nowrap">
                                {{ $highlight['count'] }}x
                                <img class="match__highlight-icon" src="{{ Voyager::image($highlight['item']->type->icon) }}" alt="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" title="{{ $highlight['item']->type->getTranslatedAttribute('title', $locale) }}" />
                            </span>
                            @endif
                        @endif
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container py-3">
        <div class="row">
            <div class="col-3">
                @foreach($match->homeId->players as $player)
                <div class="match__player w-100 d-flex justify-content-between align-items-center py-1 mb-2">
                    <div class="d-flex flex-column justify-content-start align-items-end flex-grow-1 h-100 px-2">
                       <a href="/player/{{$player->id}}">
                            <span class="text-excelsior-caps text-black">{{ mb_substr($player->getTranslatedAttribute('first_name', $locale), 0, 1) }}. {{ $player->getTranslatedAttribute('last_name', $locale) }}</span>
                       </a>
                        @unless(is_null($player->position))
                        <span class="text-excelsior-exp text-success">{{ $player->position->getTranslatedAttribute('title', $locale) }}</span>
                        @endunless
                    </div>
                    @php
                        $fullName = $player->getTranslatedAttribute('first_name', $locale) . ' ' . $player->getTranslatedAttribute('last_name', $locale);
                        $playerImage = is_null($player->image) ? url('/images/no-image.png') : Voyager::image($player->image);
                    @endphp
                    <img class="match__player__image h-100 object-fit:contain" src="{{ $playerImage }}" alt="{{ $fullName }}" title="{{ $fullName }}" />
                    <span class="match__player__position text-excelsior-caps text-gray d-flex flex-center h-100">
                        {{-- {{ str_pad((string)$player->number, 2, "0", STR_PAD_LEFT) }} --}}
                        {{ $player->getRatingByGame($match) }}
                    </span>
                </div>
                @endforeach
            </div>
            <div class="col-6">
                <div class="mb-4 position-relative">
                    <a href="{{ $match->video }}" target="_blank" rel="noopener">
                        <img class="match__video-image w-100 object-fit:cover" src="http://img.youtube.com/vi/{{ $match->videoId }}/maxresdefault.jpg">
                    </a>
                    <i class="far fa-3x fa-play-circle text-white position-absolute match__play"></i>
                </div>
                @if(count($gallery) > 0)
                <h4 class="text-excelsior-caps text-black">{{ trans('website.photo_gallery') }}</h4>
                <div class="row match__gallery">
                    @foreach($gallery as $image)
                    <div class="match__gallery__item col-4 mb-3">
                        <a href="{{ Voyager::image($image) }}" data-fancybox="match"  class="w-100 h-100">
                            <img class="w-100 h-100 object-fit:cover lozad" data-src="{{ Voyager::image($image) }}" />
                        </a>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-3">
                @foreach($match->awayId->players as $player)
                @php
                    $fullName = $player->getTranslatedAttribute('first_name', $locale) . ' ' . $player->getTranslatedAttribute('last_name', $locale);
                    $playerImage = is_null($player->image) ? url('/images/no-image.png') : Voyager::image($player->image);
                @endphp
                <div class="match__player w-100 d-flex justify-content-between align-items-center py-1 mb-2">
                    <span class="match__player__position text-excelsior-caps text-gray d-flex flex-center h-100">
                        {{-- {{ str_pad((string)$player->number, 2, "0", STR_PAD_LEFT) }} --}}
                        {{ $player->getRatingByGame($match) }}
                    </span>
                    <img class="match__player__image h-100 object-fit:contain" src="{{ $playerImage }}" alt="{{ $fullName }}" title="{{ $fullName }}" />
                    <div class="d-flex flex-column justify-content-start align-items-start flex-grow-1 h-100 px-2">
                        <span class="text-excelsior-caps text-black">{{ mb_substr($player->getTranslatedAttribute('first_name', $locale), 0, 1) }}. {{ $player->getTranslatedAttribute('last_name', $locale) }}</span>
                        @unless(is_null($player->position))
                        <span class="text-excelsior-exp text-success">{{ $player->position->getTranslatedAttribute('title', $locale) }}</span>
                        @endunless
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-md-12">
                             @php
                    $array = [
                        // '1' => 'images/banner-800x100.png',
                        '2' => 'images/800x100.png',
                    ];
                    $array_links = [
                        // '1' => 'http://sno.ge',
                        '2' =>'http://psp.ge/new/news/read/303',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp

<a href="{{$array_links[$randIndex]}}" target="_blank">
    <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
</a>
            </div>
              <div class="col-12">
                  <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
              </div>
        </div>
    </div>
</div>
@endsection
