@extends('layouts.app')


@section('content')
<section class="block container py-4">
    <h1 class="block__title text-excelsior-caps fs-38">{{ $pageTitle }}</h1>
    <div id="calendar"></div>
              @php
                    $array = [
                        // '1' => 'images/banner-800x100.png',
                        '2' => 'images/800x100.png',
                    ];
                    $array_links = [
                        // '1' => 'http://sno.ge',
                        '2' =>'http://psp.ge/new/news/read/303',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp

<a href="{{$array_links[$randIndex]}}" target="_blank">
    <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
</a>
</section>
@endsection

@push('footer-script')
<script src="{{ mix('js/calendar.js') }}"></script>
@endpush
