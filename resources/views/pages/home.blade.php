@extends('layouts.app')

@section('content')

@include('partials.slider', ['slides' => $slides])
<main class="main-block w-100" id="app">
    <div class="container">
        <div class="row reverce-mobile">
            <div class="col-md-8 col-sm-12">
                <!-- @include('partials.partners', ['partners' => $partners]) -->
 @php
                    $array = [
                         '1' => 'images/bog/730-90.png',
                    ];
                    $array_links = [
                         '1' => 'https://bankofgeorgia.ge/ka/retail/main',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp

                    <a href="{{$array_links[$randIndex]}}" target="_blank">
                        <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
                    </a>

                @include('components.news.top-news', ['news' => $mainNews])
                @include('components.poll.home-big')
                @php
                    $array = [
                        '2' => 'images/bog/axali2.png',
                    ];
                    $array_links = [
                        '2' =>'https://bankofgeorgia.ge/ka/retail/main',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp
                <div class="my-4 text-center">
                    <a href="{{$array_links[$randIndex]}}" target="_blank">
                       <img class="w-100 h-100 object-fit:cover" src="{{ url($array[$randIndex]) }}" alt="banner" title="banner">
                    </a>
                </div>
                <!-- @include('components.team.home') -->
                @include('components.news.free-news', ['freeNews' => $freeNews])
            </div>
            <div class="col-md-4 col-sm-12">
                @include('partials.sidebar', ['rating' => $topPlayers, 'tournament' => $tournament])
            </div>
        </div>
    </div>
</main>
@endsection
