@extends('layouts.app')

@section('content')
@php
$links = [
    (object)[
        'title' => trans('website.main'),
        'url' => LaravelLocalization::getLocalizedURL($locale, '/')
    ],
    (object)[
        'title' => trans('website.teams'),
        'url' => LaravelLocalization::getLocalizedURL($locale, '/clubs')
    ],
    (object)[
        'title' => $pageTitle,
    ],
];
@endphp
<div class="container">
	<div class="row">
	<div class="col-md-12">
        @include('components.breadcrumb', compact('links'))
	</div>
	<div class="col-md-12">
		<h1 class="text-excelsior-caps mb-4 fs-38">{{ $pageTitle }}</h1>
	</div>
	@foreach($stats as $key => $stat)
		<div class="col-md-6 col-sm-12">
			<h1 class="text-excelsior-caps mb-4">
				{{trans('website.statistic.' . $key . '.title')}}
			</h1>
			<div class="contain">
                @foreach($stat as $item)
                
                @if($item->player !==null)
					<div class="item position-relative">
                        <a class="overlay-link" href="{{ LaravelLocalization::getLocalizedURL($locale, '/player/' . $item['player']->id) }}"></a>
                         @php
				            $playerImage = is_null($item['player']->image) ? url('/images/no-image.png') : Voyager::image($item['player']->image);
				        @endphp
						<img src="{{ $playerImage }}" class="player-image" alt="{{$item['player']->full_name}}">
						<div class="text_wrap">
							<h5 class="text-excelsior-caps text-black font-size-1-25">{{ $item['player']->getTranslatedAttribute('first_name', $locale) }} {{ $item['player']->getTranslatedAttribute('last_name', $locale) }}</h5>
							<p>{{ $item['player']->team->name }}</p>
							<p class="fz-12 color-c">{{ $item['player']->position->title ?? '' }}</p>
						</div>
						<div class="ponter">
							<div class="text">
								{{ trans('website.statistic.' . $key . '.value') }}
							</div>
							<div class="point">
								{{ $item['value'] ?? '' }}
							</div>
						</div>
					</div>
					@endif
				@endforeach
			</div>
		</div>
	@endforeach
              <div class="col-md-12">
                  <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
              </div>
	</div>
</div>
@endsection
