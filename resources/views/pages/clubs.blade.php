@extends('layouts.app')

@section('content')
@php
$links = [
    (object)[
        'title' => trans('website.main'),
        'url' => LaravelLocalization::getLocalizedURL($locale, '/')
    ],
    (object)[
        'title' => trans('website.teams')
    ],
];
@endphp
<main class="main-block main-block--plain w-100 mb-40">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                @include('components.breadcrumb', ['links' => $links])
                <h3 class="clubs_header text-excelsior-caps">{{ trans('website.teams') }}</h3>
                <div class="row">
                    @foreach($teams as $team)
                    <div class="col-12 col-sm-4 col-md-2 position-relative">
                        <a class="club_link h-100" href="{{ LaravelLocalization::getLocalizedURL($locale, '/clubs/' . $team->id) }}"></a>
                        <div class="club_container position-relative px-2">
                            <img class="club_image object-fit:contain" src="{{ Voyager::image($team->logo) }}" alt="{{ $team->getTranslatedAttribute('name', $locale) }}" title="{{ $team->getTranslatedAttribute('name', $locale) }}">
                            <div class="text text-excelsior-caps">{{ $team->getTranslatedAttribute('name', $locale) }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
                 @php
                    $array = [
                        // '1' => 'images/banner-800x100.png',
                        '2' => 'images/800x100.png',
                    ];
                    $array_links = [
                        // '1' => 'http://sno.ge',
                        '2' =>'http://psp.ge/new/news/read/303',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp

<a href="{{$array_links[$randIndex]}}" target="_blank">
    <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
</a>
            </div>
        </div>
    </div>
</main>
@endsection
