@extends('layouts.app')


@section('content')
    @php
        $links = [
            (object)[
                'title' => trans('website.main'),
                'url' => LaravelLocalization::getLocalizedURL($locale, '/')
            ],
            (object)[
                'title' => trans('website.teams'),
                'url' => LaravelLocalization::getLocalizedURL($locale, '/clubs')
            ],
            (object)[
                'title' => $pageTitle,
            ],
        ];
    @endphp
    <div class="container">
        @include('components.breadcrumb', compact('links'))
    </div>
    <section class="block container">
        <h1 class="block__title text-excelsior-caps fs-38">{{ $pageTitle }}</h1>
        @unless(is_null($tournament))
        <div class="tournament-widget border border-gray mb-5">
            <div class="tournament-widget__content">
                <ul class="nav nav-tabs overflow-auto d-flex justify-content-start align-items-center flex-nowrap border-0 p-3" role="tablist">
                    <li class="nav-item px-0">
                        <a href="/cxrili" class="nav-link py-1 px-2 mr-2 nav-link--group text-excelsior-caps">
                            ფინალური ეტაპი
                        </a>
                    </li>
                    @foreach($tournament as $item)
                    <li class="nav-item px-0">
                        <a class="nav-link py-1 px-2 mr-2 nav-link--group text-excelsior-caps {{$loop->iteration == 1 ? 'active':''}}" data-toggle="tab" href="#tournament-group-{{ strtolower($item['group']->title) }}" role="tab" aria-controls="tournament-group-{{ strtolower($item['group']->title) }}" aria-selected="true">{{ $item['group']->title }}</a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                @foreach($tournament as $item)
                <div class="tab-pane fade {{$loop->iteration == 1 ? 'show active':''}} tournament-widget__table" id="tournament-group-{{ strtolower($item['group']->title) }}">
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="text-excelsior-caps font-size-sm border-0 text-center" scope="col">#</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">{{ trans('website.team') }}</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">თ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">მ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">ფ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">წ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">გ/გ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">გ/გ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">+/-</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">ქ</th>
                                <th class="text-excelsior-caps font-size-sm border-0 text-center" scope="col">{{ trans('website.matches') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item['profiles'] as $clubs)
                           
                            <tr class="bg-light-gray">
                                <th scope="row" class="text-center">{{ $loop->iteration }}</th>
                                <td>
                                    <a class="text-dark-blue text-excelsior-caps fs-12" href="{{ LaravelLocalization::getLocalizedURL($locale, '/clubs/' . $clubs->id) }}">
                                        <img src="{{ asset('storage/' . $clubs->logo) }}" class="tournament-widget__team__icon object-fit:contain mr-2"/>{{ $clubs->name }}
                                    </a>
                                </td>
                                <td>{{ $clubs['info']['wins'] + $clubs['info']['draws'] + $clubs['info']['loose'] }}</td>
                                <td>{{ $clubs['info']['wins'] }}</td>
                                <td>{{ $clubs['info']['draws'] }}</td>
                                <td>{{ $clubs['info']['loose'] }}</td>
                                <td>{{ $clubs['info']['goals_out'] }}</td>
                                <td>{{ $clubs['info']['goals_in'] }}</td>
                                <td>
                                    {{ $clubs->goals }}
                                </td>
                                <td>{{ $clubs->points }}</td>
                                <td class="text-center px-2">
                                    @foreach($clubs['info']['last_games'] as $game)
                                    @php
                                        $isHome = $game->home_id === $clubs['id'];
                                        $score = $game->home_score - $game->away_score;
                                        if ($score === 0) {
                                            $result = 'draw';
                                        } elseif($score < 0) {
                                            $result = !$isHome && $score < 0 ? 'win' : 'lose';
                                        }
                                        else {
                                            $result = $isHome && $score > 0 ? 'win' : 'lose';
                                        }
                                    @endphp
                                    <a class="history-result history-result--{{ $result }} d-inline-block text-excelsior-caps text-white text-center mr-2" href="{{ LaravelLocalization::getLocalizedURL($locale, '/match/' . $game->id) }}">
                                        {{ trans('website.' . $result . '_short') }}
                                    </a>
                                    @endforeach
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endforeach
            </div>
            </div>
        </div>
        @endunless
    </section>
@endsection
