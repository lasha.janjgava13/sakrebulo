@extends('layouts.app')

@php
$links = [
    (object)[
        'title' => trans('website.main'),
        'url' => LaravelLocalization::getLocalizedURL($locale, '/')
    ],
    (object)[
        'title' => trans('website.gallery'),
    ],
];
@endphp
@section('content')
<main class="container">
	<div class="row">
        <div class="col-12">
            @include('components.breadcrumb', compact('links'))
        </div>
    </div>
    <div class="row">
        @foreach($galleries as $gallery)
        @php
            $images = json_decode($gallery->images);
        @endphp
        @if(count($images) > 0)
        <div class="col-12 col-md-4 mb-3">
            <div class="gallery position-relative">
                <a href="{{ Voyager::image($images[0]) }}" data-fancybox="gallery-{{ $gallery->id }}" class="overlay-link"></a>
                <img class="gallery__image w-100 h-100 object-fit:cover lozad" data-src="{{ Voyager::image($images[0]) }}" alt="{{ $gallery->getTranslatedAttribute('title', $locale) }}" title="{{ $gallery->getTranslatedAttribute('title', $locale) }}" />
                <div class="gallery__title w-100 d-flex justify-content-between align-items-center text-excelsior-caps text-white">
                    <span>{{ $gallery->getTranslatedAttribute('title', $locale) }}</span>
                    <span class="text-excelsior-caps"><i class="fas fa-camera mr-1"></i>{{ count($images) }}</span>
                </div>
            </div>
            @php(array_shift($images))
            @foreach($images as $image)
                <a href="{{ Voyager::image($image) }}" data-fancybox="gallery-{{ $gallery->id }}" class="d-none"></a>
            @endforeach
        </div>
        @endif
        @endforeach
        <div class="col-12">
            {{ $galleries->links() }}
        </div>
    </div>
</main>
@endsection
