@extends('layouts.app')

@section('content')
    @php
        $links = [
            (object)[
                'title' => trans('website.main'),
                'url' => LaravelLocalization::getLocalizedURL($locale, '/')
            ],
            (object)[
                'title' => trans('website.teams'),
                'url' => LaravelLocalization::getLocalizedURL($locale, '/clubs')
            ],
            (object)[
                'title' => $player->team->getTranslatedAttribute('name', $locale),
                'url' => LaravelLocalization::getLocalizedURL($locale, '/clubs/' . $player->team->id)
            ],
            (object)[
                'title' => $pageTitle,
            ],
        ];
    @endphp
    <div class="container">
        @include('components.breadcrumb', compact('links'))
    </div>
    <section class="player container">
        <div class="row">
            <div class="col-12 col-md-3">
                <img class="player__image w-100 object-fit:contain" src="{{ Voyager::image($player->image) }}" title="{{ $player->fullName }}" alt="{{ $player->fullName }}" />
            </div>
            <div class="col-12 col-md-5">
                <div class="d-flex justify-content-start align-items-start mb-3">
                    <img class="player__team object-fit:contain mr-2" src="{{ Voyager::image($player->team->logo) }}" title="{{ $player->team->getTranslatedAttribute('name', $locale) }}" alt="{{ $player->team->getTranslatedAttribute('name', $locale) }}" />
                    <h4 class="fs-18 text-excelsior-caps text-dark-blue">{{ $player->team->getTranslatedAttribute('name', $locale) }}</h4>
                </div>
                @unless(is_null($player->birth_date))
                <p class="d-block text-gray text-excelsior-caps fs-18 mb-2">
                    @php
                        $age = Carbon\Carbon::today()->diffInYears(Carbon\Carbon::parse($player->birth_date));
                    @endphp
                    <span class="mr-2">{{ trans('website.age') }}</span> {{ $age }}
                </p>
                @endunless
                <div class="d-flex justify-content-start align-items-center flex-wrap mb-3">
                    <h1 class="text-black text-excelsior-caps fs-32 mb-0 mr-2">{{ $player->fullName }}</h1>
                    {{-- <span class="text-gray text-excelsior-caps fs-32"># {{ str_pad((string)$player->number, 2, "0", STR_PAD_LEFT) }}</span> --}}
                </div>
                <div class="d-flex justify-content-start align-items-start mb-3">
                    <div class="player__rating bg-success py-1 px-2 text-center mr-3">
                        <span class="d-block w-100 mb-1 fs-14 text-white text-excelsior-caps opacity-50">{{ trans('website.rating') }}</span>
                        <span class="text-white text-excelsior-caps fs-32">{{ number_format($player->getRating(), 1) }}</span>
                    </div>
                   <div class="d-flex justify-content-start align-items-start mb-3" style="flex-direction: column;">
                        <div class="py-2">
                        <span class="text-success text-excelsior-exp">{{ $player->position->getTranslatedAttribute('title', $locale) }}</span>
                        </div>

                        <div>
                             <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="player__highlight py-2 col-12 col-md-3 d-flex flex-column justify-content-between align-items-center">
                        <span class="text-excelsior-caps text-dark-blue">{{ trans('website.matches') }}</span>
                        <span class="text-excelsior-caps text-black">{{ $player->getPlayerGamesCount() }}</span>
                    </div>
                    <div class="player__highlight py-2 col-12 col-md-3 d-flex flex-column justify-content-between align-items-center">
                        <span class="text-excelsior-caps text-dark-blue">{{ trans('website.cards') }}</span>
                        <div class="w-100 d-flex justify-content-between align-items-center">
                            <span class="d-flex flex-center text-black text-excelsior-caps">
                                <img class="mr-2"
                                    src="{{ url('images/icons/yellow-card.jpg') }}"
                                    srcset="{{ url('images/icons/yellow-card@2x.jpg') }} 2x, {{ url('images/icons/yellow-card@3x.jpg') }} 3x"
                                    alt="{{ trans('website.yellow_card') }}"
                                    title="{{ trans('website.yellow_card') }}"
                                    />
                                {{ $highlights->yellow_card }}
                            </span>
                            <span class="d-flex flex-center text-black text-excelsior-caps">
                                <img class="mr-2"
                                    src="{{ url('images/icons/red-card.jpg') }}"
                                    srcset="{{ url('images/icons/red-card@2x.jpg') }} 2x, {{ url('images/icons/red-card@3x.jpg') }} 3x"
                                    alt="{{ trans('website.red_card') }}"
                                    title="{{ trans('website.red_card') }}"
                                    />
                                {{ $highlights->red_card }}
                            </span>
                        </div>
                    </div>
                    <div class="player__highlight py-2 col-12 col-md-3 d-flex flex-column justify-content-between align-items-center">
                        <span class="text-excelsior-caps text-dark-blue">{{ trans('website.goals') }}</span>
                        <span class="d-flex flex-center">
                            <img class="mr-2"
                                src="{{ url('images/icons/goal.png') }}"
                                srcset="{{ url('images/icons/goal@2x.png') }} 2x, {{ url('images/icons/goal@3x.png') }} 3x"
                                alt="{{ trans('website.goals') }}"
                                title="{{ trans('website.goals') }}"
                                />
                            {{ $highlights->goal }}
                        </span>
                    </div>
                    <div class="player__highlight py-2 col-12 col-md-3 d-flex flex-column justify-content-between align-items-center">
                        <span class="text-excelsior-caps text-dark-blue">{{ trans('website.assists') }}</span>
                        <span class="d-flex flex-center text-black text-excelsior-caps">
                            <img class="mr-2"
                                src="{{ url('images/icons/assist.png') }}"
                                srcset="{{ url('images/icons/assist@2x.png') }} 2x, {{ url('images/icons/assist@3x.png') }} 3x"
                                alt="{{ trans('website.assists') }}"
                                title="{{ trans('website.assists') }}"
                                />
                            {{ $highlights->assist }}
                        </span>
                    </div>
                </div>
             <!--    <div class="w-100">
                    <h4 class="text-dark-blue text-excelsior-caps fs-16 mb-2">{{ trans('website.information') }}</h4>
                    <div class="text-excelsior-caps">{!! $player->getTranslatedAttribute('text', $locale) !!}</div>
                </div> -->
            </div>
            <div class="col-12 col-md-4">
                <div class="player__matches">
                    <div class="player__matches__head d-flex flex-center bg-dark-blue px-3">
                        <span class="text-excelsior-caps text-white fs-16">{{ trans('website.matches') }}</span>
                    </div>
                    <div class="player__matches__body">
                        @foreach($matches as $match)
                        <div class="player__match d-flex flex-center bg-light-gray">
                            <div class="player__match__block h-100 px-2 text-truncate">
                                <span class="text-dark-blue text-excelsior-caps fs-12">{{ $match->isHome ? $player->team->getTranslatedAttribute('name', $locale) : $match->awayId->getTranslatedAttribute('name', $locale) }}</span>
                            </div>
                            <div class="player__match__block player__match__block--result h-100 px-2 text-center">
                                <span class="text-black text-excelsior-caps fs-16">{{ $match->home_score }} - {{ $match->away_score }}</span>
                            </div>
                            <div class="player__match__block h-100 px-2 text-truncate">
                                <span class="text-dark-blue text-excelsior-caps fs-12">{{ $match->isHome ? $match->awayId->getTranslatedAttribute('name', $locale) : $player->team->getTranslatedAttribute('name', $locale) }}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                  @php
        $array = [
            // '1' => 'images/banner-390x500.jpg',
            '2' => 'images/bog/350 x 450.png',
        ];
        $array_links = [
            // '1' => 'http://sno.ge',
            '2' =>'https://bankofgeorgia.ge/ka/retail/main',
        ];
        $randIndex = array_rand($array, 1);
    @endphp

    <a href="{{$array_links[$randIndex]}}" target="_blank">
        <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
    </a>

            </div>
            <div class="col-12">
                  <div class="fb-share-button"
                    data-href="{{ Request::url() }}"
                    data-layout="button_count">
                  </div>
            </div>
              <div class="col-12">
                  <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
              </div>
        </div>
    </section>
@endsection
