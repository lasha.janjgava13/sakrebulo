@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Manage Game')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
        	Manage Match
        </h1>
    </div>
@stop
@section('content')
<form action="{{ action('CustomController@postGame') }}" method="post">
	<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
	<input type="hidden" name="matcher" value="{{$match->id}}">

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h3 class="crf">
					Home {{ $match->homeId()->first()->name}}
					<div class="div pull-right">
						<input type="number" name="home_score" value="{{$match->home_score}}" class="form-control">
					</div>
				</h3>
				<div class="col-md-12">
					<h4>Players :</h4>
					<table class="table">
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th>
								Statistic
							</th>
						</tr>
						@foreach($match->homeId->players as $player)
						<tr>
							<td>
								{{ $player->number }}
							</td>
							<td>
								{{ $player->first_name." ".$player->last_name }}
							</td>
							<td>
								<input type="number" name="statistic[{{ $match->id }}][{{ $player->id }}]" step="0.1" value="<?=!empty($statistic[$match->id][$player->id])?$statistic[$match->id][$player->id]->value:6.5?>" class="form-control" style="width:auto;">

							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
			<div class="col-md-6">
					<h3 class="crf">
					Away {{ $match->awayId()->first()->name}}
					<div class="div pull-left">
						<input type="number" name="away_score" value="{{$match->away_score}}" class="form-control">
					</div>
				</h3>
				<div class="col-md-12">
					<h4>Players :</h4>
					<table class="table">
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th>
								Statistic
							</th>
						</tr>
					@foreach($match->awayId->players as $player)
							<tr>
							<td>
								{{ $player->number }}
							</td>
							<td>
								{{ $player->first_name." ".$player->last_name }}
							</td>
							<td>
								<input type="number" name="statistic[{{ $match->id }}][{{ $player->id }}]" step="0.1" value="<?=!empty($statistic[$match->id][$player->id])?$statistic[$match->id][$player->id]->value:6.5?>" class="form-control" style="width:auto;">
							</td>
						</tr>
					@endforeach
				</table>
				</div>
			</div>
			<div class="col-md-12">
				<input type="submit" name="action" value="Statistic" class="btn btn-primary">
				<input type="submit" name="action" value="Score" class="btn btn-primary">
			</div>
			<div class="col-md-12">
				<ul>
					@foreach($highlights as $highlight)
					<li>
						{{$highlight->title }} {{$highlight->player->first_name." ".$highlight->player->last_name}} {{$highlight->team->name}}
						{{$highlight->type->title}}
						{{$highlight->time}}
					</li>
				@endforeach
				</ul>
				<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
		  Add highlight
		</button>
			</div>
		</div>
	</div>
</form>


<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Highlight</h4>
      </div>
     <form action="{{ action('CustomController@postGame') }}" id="modal_form" method="post">
		<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
		<input type="hidden" name="match_id" value="{{$match->id}}">
		<input type="hidden" name="action" value="highlights">
      		<div class="col-md-12">
      			<div class="form-group">
		      		<label for="title">Enter Title</label>
		      		<input type="text" id="title" class="form-control" name="title">
		      	</div>
		      	<div class="form-group">
		      		<label for="time">time</label>
		      		<input type="number" id="time" class="form-control"  name="time">
		      	</div>
		      	<div class="form-group">
		      		<label for="player">Select Player</label>
		      		<select name="player_id" id="player" class="form-control">
						@foreach($players as $player)
						<option value="{{$player->id}}">{{$player->first_name." ".$player->last_name}}</option>
						@endforeach
		      		</select>
		      	</div>
		      	<div class="form-group">
		      		<label for="type_id">Select Highlight Type</label>
		      		<select name="type_id" id="type_id" class="form-control">
						@foreach($types as $type)
						<option value="{{$type->id}}">{{$type->title}}</option>
						@endforeach
		      		</select>
		      	</div>
		      	<div class="form-group">
		      		<label for="team_id">Select Team</label>
		      		<select name="team_id" id="team_id" class="form-control">
						@foreach($teams as $team)
						<option value="{{$team->id}}">{{$team->name}}</option>
						@endforeach
		      		</select>
		      	</div>
      		</div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="save_modal_form">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
@stop

@section('css')
<style>
	.crf{
		text-align: center;
	}
	.logo{
		width: 35px;
	}
</style>
@stop

@section('javascript')
<script>
	$('#save_modal_form').click(function(e){
      $('#modal_form').submit();
});
</script>
@stop
