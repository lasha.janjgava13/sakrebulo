<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta property="fb:app_id" value="381460115634835"/>
    @if(isset($pageTitle) && !is_null($pageTitle))
    <title>{{ $pageTitle }} - {{ setting('site.title') }}</title>
    @else
    <title>{{ setting('site.title') }}</title>
    @endif
    {!! MetaTag::openGraph() !!}
    {!! MetaTag::tag('title',setting('site.title')) !!}
    <meta property="og:image" content="{{ asset('images/share.jpg') }}">
    <meta property="og:title" content="{{ setting('site.title') }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.translations = JSON.parse('@json(trans('website'))');
    </script>
    <script src="{{ url('/js/manifest.js') }}"></script>
    <script src="{{ url('/js/vendor.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
     <style type="text/css">
                .bg-dark-blue {
                    background-color: {{{setting('site.sitecolor') }}};
                }
                .main-block::after{
                    background-color: {{{setting('site.sitecolor') }}};
                }
            </style>

</head>
<body class="pb-0">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0" nonce="mLvyf5IY"></script>
    <div class="sh-gh" style="position: absolute; top:-500px;left:-500px z-index:-100" >
        <div class="cc">
            <div class="fb-like" data-href="https://www.facebook.com/minifootball.com.ge" data-width="100" data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
        </div>
    </div>
    @include('components.team.top', ['teams' => $teams])
    @include('partials.topmenu')

    @yield('content')

 
    @include('partials.footer')

    <div class="social-networks">
        @if(setting('site.facebook_page'))
        <div class="social-network w-100 mb-2 d-flex justify-content-start align-items-center position-relative text-white">
            <a href="{{ setting('site.facebook_page') }}" target="blank" rel="noopener" class="overlay-link"></a>
            <div class="social-network__icon">
                <i class="fab fa-facebook-f"></i>
            </div>
            <span class="text-excelsior-caps">{{ trans('website.facebook') }}</span>
        </div>
        @endif
        @if(setting('site.youtube_page'))
        <div class="social-network w-100 mb-2 d-flex justify-content-start align-items-center position-relative text-white">
            <a href="{{ setting('site.youtube_page') }}" target="blank" rel="noopener" class="overlay-link"></a>
            <div class="social-network__icon">
                <i class="fab fa-youtube"></i>
            </div>
            <span class="text-excelsior-caps">{{ trans('website.youtube') }}</span>
        </div>
        @endif
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.8.2/js/all.js" integrity="sha384-DJ25uNYET2XCl5ZF++U8eNxPWqcKohUUBUpKGlNLMchM7q4Wjg2CUpjHLaL8yYPH" crossorigin="anonymous"></script>
    @stack('footer-script')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', "{{ setting('site.google_analytics_tracking_id') }}");
    </script>
</body>
</html>
