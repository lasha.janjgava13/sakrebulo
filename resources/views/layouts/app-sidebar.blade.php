@extends('layouts.app')

@section('content')
<main class="main-block main-block main-block--plain @yield('main-block-class')">
    <div class="container @yield('container-class')">
        <div class="row">
            <div class="col-md-8">
                @yield('left')
            </div>
            <div class="col-md-4">
                @yield('right')
            </div>
        </div>
    </div>
</main>
@endsection
