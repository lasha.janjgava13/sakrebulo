<div id="main-slider" class="c-slider">
    @foreach($slides as $slide)
    <div class="c-slider__slide w-100 overflow-hidden" style="background-image: url('{{ Voyager::image($slide->image) }}')">
        <div class="container pt-5">
            <div class="row">
                <div class="col-md-6">
                    <time class="d-block text-white text-excelsior-caps mb-4">{{ $slide->date->format('d') }} {{ trans('calendar.' . strtolower($slide->date->format('F'))) }}</time>
                    {{-- <a href="#" class="px-2 py-1 text-white text-excelsior-caps bg-success">კატეგორია</a> --}}
                    <h3 class="c-slider__slide__title text-excelsior-caps text-white mt-3 mb-3">{{ $slide->title }}</h3>
                    <div class="py-2">
                        {!! $slide->text !!}
                    </div>
                    @if($slide->url)
                    <a class="c-slider__slide__more text-excelsior-caps text-white pb-1 px-1" href="{{ $slide->url }}">{{ trans('website.more') }}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
