<div class="containe pt-30">
	<div class="row">
		<div class="col-md-12">
			@foreach($news as $new)
				<div class="news-list position-relative">
					<div class="news-list__item">
						<img src="{{asset('storage/'.$new->image)}}" class='news-list__item--image' alt="">
						<div class="news_list__box">
							<div class="mb-2">
		                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">კატეგორია</a>
		                        <time class="text-black text-excelsior-caps">25 მარტი</time>
		                    </div>
		                    <h4 class="new_list_item--header text-black text-excelsior-caps">
		                    	{{ $new->title }}
		                    </h4>
		                    	{!! $new->description !!}
		                     <a href="{{ '/news/'.$new->slug }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">მეტი</a>
						</div>
					</div>
				</div>
			@endforeach
			{{ $news->links('vendor.pagination.bootstrap-4')}}
		</div>
	</div>
</div>