<footer class="bg-darker-blue">
    <div class="footer container py-5">
        <div class="row justify-content-between align-items-start">
            <div class="footer__column">
               <img class="mr-2" alt="{{ setting('site.title') }}" title="{{ setting('site.title') }}" height="90px" src="/storage/{{ setting('site.logo') }}" />
            </div>
            <div class="footer__column">
                <span class="text-dusky-blue text-excelsior-caps">Copyright © 2019 ყველა უფლება დაცულია</span>
            </div>
        </div>
    </div>
</footer>
