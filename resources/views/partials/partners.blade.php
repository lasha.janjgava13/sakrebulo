@if($partners->count() > 0)
<div class="c-partners d-flex justify-content-center align-items-center w-100 my-3 py-2">
    @foreach($partners->where('type', 'partner') as $partner)
    <img class="h-100 object-fit:cover" alt="{{ $partner->title }}" title="{{ $partner->title }}" src="{{ Voyager::image($partner->image) }}" />
    @endforeach
</div>
@endif
