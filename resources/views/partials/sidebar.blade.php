<div class="sidebar sidebar--home">
    @if(!empty($rating))
    <div class="players-tab mb-5">
        <ul class="nav nav-tabs overflow-hidden d-flex justify-content-space-between align-items-center flex-nowrap border-0" id="playersTab" role="tablist">
            <li class="nav-item flex-grow-1 px-0">
                <a class="nav-link px-2 nav-link--player text-excelsior-caps text-white border-0 rounded-0 active" id="top-player-tab" data-toggle="tab" href="#top-player" role="tab" aria-controls="top-player" aria-selected="true">რეიტინგი</a>
            </li>
            <li class="nav-item flex-grow-1 px-0 px-1">
                <a class="nav-link px-2 nav-link--player text-excelsior-caps text-white border-0 rounded-0" id="top-goaler-tab" data-toggle="tab" href="#top-goaler" role="tab" aria-controls="top-goaler" aria-selected="false">გოლი</a>
            </li>
            <li class="nav-item flex-grow-1 px-0">
                <a class="nav-link px-2 nav-link--player text-excelsior-caps text-white border-0 rounded-0" id="top-kicker-tab" data-toggle="tab" href="#top-kicker" role="tab" aria-controls="top-kicker" aria-selected="false">ასისტი</a>
            </li>
        </ul>
        <div class="tab-content bg-white" id="playersTabContent">
            <div class="tab-pane fade show active" id="top-player" role="tabpanel" aria-labelledby="top-player-tab">
                @foreach($rating->player as $item)
                @include('components.player.playerStat', ['item' => $item, 'type' => 'rating'])
                @endforeach
            </div>
            <div class="tab-pane fade" id="top-goaler" role="tabpanel" aria-labelledby="top-goaler-tab">
                @foreach($rating->goaler as $item)
                @include('components.player.playerStat', ['item' => $item, 'type' => 'goals'])
                @endforeach
            </div>
            <div class="tab-pane fade" id="top-kicker" role="tabpanel" aria-labelledby="top-kicker-tab">
                @foreach($rating->assistant as $item)
                @include('components.player.playerStat', ['item' => $item, 'type' => 'assists'])
                @endforeach
            </div>
        </div>
    </div>
    @endif
    @unless(is_null($sidebarVideo))
    <div class="video-widget position-relative cursor-pointer mb-5">
        <a href="{{ $sidebarVideo->url }}" class="video-widget__link overlay-link"></a>
        <img src="{{ $sidebarVideo->imageUrl }}" class="video-widget__image position-absolute w-100 h-100">
        <div class="video-widget__play position-absolute"></div>
        <div class="video-widget__title-block w-100 position-absolute">
            <span class="video-widget__title text-white text-excelsior-caps">{{ $sidebarVideo->getTranslatedAttribute('title', $locale) }}</span>
        </div>
    </div>
    @endunless
    @unless(is_null($tournament))
    <div class="tournament-widget mb-5">
        <div class="tournament-widget__title bg-dark-blue py-3">
            <h5 class="text-white text-center text-excelsior-caps mb-0">{{ trans('website.tournament_table') }}</h5>
        </div>
        <div class="tournament-widget__content px-3">
            <ul class="nav nav-tabs overflow-auto d-flex justify-content-start align-items-center flex-nowrap border-0 py-2" role="tablist">
                @foreach($tournament as $item)
                <li class="nav-item px-0">
                    <a class="nav-link py-1 px-2 mr-2 nav-link--group text-excelsior-caps {{$loop->iteration == 1 ? 'active':''}}" data-toggle="tab" href="#tournament-group-{{ strtolower($item['group']->title) }}" role="tab" aria-controls="tournament-group-{{ strtolower($item['group']->title) }}" aria-selected="true">{{ $item['group']->title }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content">
                 @foreach($tournament as $item)
                <div class="tab-pane fade {{$loop->iteration == 1 ? 'show active':''}} tournament-widget__table" role="tabpanel" id="tournament-group-{{ strtolower($item['group']->title) }}">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-excelsior-caps font-size-sm border-0 text-center" scope="col">#</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">{{ trans('website.team') }}</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">თ</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">+/-</th>
                                <th class="text-excelsior-caps font-size-sm border-0" scope="col">ქ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item['profiles'] as $clubs)
                            <tr class="bg-light-gray">
                                <th scope="row" class="text-center">
                                    {{ $loop->iteration }}
                                </th>
                                <td>
                                    <a class="text-dark-blue text-excelsior-caps fs-12" href="{{ LaravelLocalization::getLocalizedURL($locale, '/clubs/' . $clubs['team']->id) }}">
                                        <img src="{{ asset('storage/' . $clubs['team']->logo) }}" class="tournament-widget__team__icon object-fit:contain mr-2"/>{{ $clubs['team']->name }}
                                    </a>
                                </td>
                                <td>{{ $clubs['wins'] + $clubs['draws'] + $clubs['loose'] }}</td>
                                <td>
                                    @if(($clubs['goals_out'] - $clubs['goals_in']) > 0)
                                        +{{ $clubs['goals_out'] - $clubs['goals_in'] }}
                                    @else
                                        {{ $clubs['goals_out'] - $clubs['goals_in'] }}
                                    @endif
                                </td>
                                <td>{{ $clubs['point'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    @endunless
    <a href="https://bankofgeorgia.ge/ka/retail/main">
        <img src="{{ url('/images/bog/350 x 300.png') }}" style="width:100%;height:300px;" alt="">
    </a>
</div>
