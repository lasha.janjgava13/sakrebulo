<nav class="d-none d-sm-block c-menu bg-dark-blue w-100">
    <div class="container h-100">
        <div class="d-flex justify-content-space-between align-items-center h-100">
            <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/') }}" class="c-menu__logo h-100 d-flex justify-content-start align-items-center">
                <img class="mr-2" alt="{{ setting('site.title') }}" title="{{ setting('site.title') }}" height="90px" style="width:100px;margin-bottom:10px" src="/storage/{{ setting('site.logo') }}" />

                <div class="d-flex justify-content-start align-items-center flex-wrap mr-2">
                    <h1 class="c-menu__logo__title w-100 text-white text-excelsior-caps">{{ setting('site.title') }}</h1>
                    <h4 class="c-menu__logo__caption text-excelsior-caps  w-100">{{ trans('website.official_website') }}</h4>
                </div>
            </a>
            <div class="c-menu__right h-100 flex-grow-1">
                <div class="d-flex justify-content-between align-items-center flex-wrap w-100 h-100">
                    <div class="c-menu__partners d-none d-sm-flex justify-content-start align-items-center w-100 h-50 pl-4">
                        <div class="flex-grow-1">
                            @foreach($partners->where('type', 'organizer') as $partner)
                            <a href="{{ is_null($partner->url) ? '#' : $partner->url }}" class="c-menu__partners__partner">
                                <img class="c-menu__partners__image" src="{{ Voyager::image($partner->image) }}"  alt="{{ $partner->title }}" title="{{ $partner->title }}" />
                            </a>
                            @endforeach
                            
                        </div>
                        <div class="c-menu__auth">
                            @if(Auth::check())
                             {{-- <div class="c-menu__logo__title w-100 text-white text-excelsior-caps" style="display: inline;">
                                 {{ Auth::User()->name }}
                             </div>
                             <a class="btn btn-warning text-excelsior-caps rounded-1" href="/logout">{{ trans('website.logout') }}</a> --}}
                            @else
                             {{-- <a class="btn btn-facebook text-excelsior-caps rounded-1" href="/auth/redirect/facebook">
                              <img src="{{asset('images/f_logo_RGB-Blue_100.png')}}" alt="" style="height: 30px">  {{ trans('website.login') }}
                            </a> --}}
                            @endif
                            <div class="btn-group">
                                <button type="button" class="btn btn-transparent text-white rounded-0 dropdown-toggle text-excelsior-caps" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ strtoupper(app()->getLocale()) }}
                                </button>
                                @if(count(LaravelLocalization::getSupportedLocales()) > 0)
                                <div class="dropdown-menu">
                                    @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <a class="dropdown-item text-excelsior-caps {{ LaravelLocalization::getCurrentLocale() == $localeCode ? 'disabled' : '' }}" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{ menu('main', 'components.topmenu') }}
                </div>
            </div>
        </div>
    </div>
</nav>

<div class="container d-block d-sm-none bg-dark-blue">
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark-blue">
               <a class="navbar-brand" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="56.098" height="28.11" viewBox="0 0 56.098 28.11">
                  <g id="Group_21" data-name="Group 21" transform="translate(-16 -24.217)">
                    <g id="Group_1" data-name="Group 1" transform="translate(16 24.272)">
                      <path id="Path_1" data-name="Path 1" d="M87.3,49.391v7.693a.952.952,0,0,0,.308.7L100.454,69.8a.962.962,0,0,0,1.329,0l.266-.266a.957.957,0,0,0,0-1.35l-10.5-10.511a.962.962,0,0,1-.175-1.106,9.911,9.911,0,0,0,.707-4.211V48.684a.957.957,0,0,0-.532-.851l-10.936-5.71a.957.957,0,0,0-1.239.314h0a.957.957,0,0,0,.351,1.377l7.279,3.855a.952.952,0,0,1,.4,1.249v.027a.909.909,0,0,0-.112.447Z" transform="translate(-78.423 -42.014)" fill="#fff"/>
                      <path id="Path_2" data-name="Path 2" d="M88.231,65.155l2.127-1.685v2.9l-.4.239a1.276,1.276,0,0,1-1.638-.271l-.17-.2a.686.686,0,0,1,.08-.978Z" transform="translate(-82.527 -52.062)" fill="#fff"/>
                      <path id="Path_3" data-name="Path 3" d="M101.42,47.162l1.372.893a.574.574,0,0,1,.25.468v.766s2.127,0,2.658-3.19-2.658-2.658-2.658-2.658a5.848,5.848,0,0,0-1.622,3.722Z" transform="translate(-88.82 -42.667)" fill="#fff"/>
                      <path id="Path_4" data-name="Path 4" d="M86.068,73.47l3.19,2.658s0,1.6-1.063,1.6H77.9s-.867-2.127,1.26-2.127,6.38-.367,6.38-.367Z" transform="translate(-77.721 -56.746)" fill="#fff"/>
                      <circle id="Ellipse_2" data-name="Ellipse 2" cx="2.924" cy="2.924" r="2.924" transform="translate(24.296 21.509)" fill="#fff"/>
                    </g>
                    <path id="Path_35" data-name="Path 35" d="M.186,124H9.038c1.092,0,1.142-.708,1.142-1.092s-.273-1.006-1.241-1.241l-6.207-1.875c-2.11-.67-2.731-2.3-2.731-4.531s1.055-4.047,3.724-4.047h9.857v3.2H5.3c-.6,0-1.13.223-1.13,1.043,0,.621.323,1.055,1.241,1.353l5.661,1.589c2.371.683,3.3,1.875,3.3,4.494s-.931,4.246-3.65,4.246H.186Z" transform="translate(40 -86.924)" fill="#fff"/>
                    <path id="Path_41" data-name="Path 41" d="M113.574,127.187a2.148,2.148,0,0,1-2.408-2.483V111.21h4.1v11.719c0,.795.286,1.068,1.08,1.068h8.28v3.191Z" transform="translate(-52.525 -86.993)" fill="#fff"/>
                  </g>
                </svg>

              </a>
               <div class="btn-group">
                    <button type="button" class="btn btn-transparent text-white rounded-0 dropdown-toggle text-excelsior-caps" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ strtoupper(app()->getLocale()) }}
                    </button>
                    @if(count(LaravelLocalization::getSupportedLocales()) > 0)
                    <div class="dropdown-menu">
                        @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <a class="dropdown-item text-excelsior-caps {{ LaravelLocalization::getCurrentLocale() == $localeCode ? 'disabled' : '' }}" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            {{ $properties['native'] }}
                        </a>
                        @endforeach
                    </div>
                    @endif
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                </div>
              
               
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    {{ menu('main', 'components.topmenumobile') }}
              </div>
            </nav>
        </div>
    </div>
</div>
