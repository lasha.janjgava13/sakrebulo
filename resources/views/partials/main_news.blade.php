<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item text-excelsior-caps" ><a href="/">{{ trans('website.main')}}</a></li>
    <li class="breadcrumb-item text-excelsior-caps active" aria-current="page">{{ trans('website.news') }}</li>
  </ol>
</nav>
<h2 class="news_header text-excelsior-caps">{{ trans('website.news') }} </h2>
<div class="containe">
	<div class="row">
		<div class="col-md-12 col-xs-12 pr-0">
            <div class="top-news position-relative">
                <img class="top-news__image main_news__image w-60 pull-left h-100" src="{{ url('/images/demo/news/news-1.png') }}" alt="news title" title="news title">
                <div class="main-news__block top-news__block__title h-100 d-flex flex-column justify-content-start align-items-start">
                    <div class="mb-2">
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">კატეგორია</a>
                        <time class="text-black text-excelsior-caps">25 მარტი</time>
                    </div>
                    <h4 class="top-news__title mb-3 text-excelsior-caps text-black">საუკეთესო კადრები ბოლო თამაშის</h4>
                    <a href="#" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">მეტი</a>
                </div>
            </div>
		</div>
	</div>
</div>