@php
	$array = [
		// '1' => 'images/banner-390x500.jpg',
		'2' => 'images/bog/350 x 450.png',
	];
	$array_links = [
		// '1' => 'http://sno.ge',
		'2' =>'https://bankofgeorgia.ge/ka/retail/main',
	];
	$randIndex = array_rand($array, 1);
@endphp

<a href="{{$array_links[$randIndex]}}" target="_blank">
    <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
</a>
