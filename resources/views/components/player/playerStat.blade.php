@php
$player = $item['player'];
@endphp
@if(!empty($player))
    <a class="" href="{{ LaravelLocalization::getLocalizedURL($locale, '/player/' . $player->id) }}">
<div class="top-player d-flex justify-content-center align-items-start position-relative">
    <div class="top-player__image h-100">
        @php
            $playerImage = is_null($player->image) ? url('/images/no-image.png') : Voyager::image($player->image);
        @endphp
        <img class="w-100 h-100 object-fit:cover" src="{{ $playerImage }}" alt="{{ $player->getTranslatedAttribute('first_name', $locale) }} {{ $player->getTranslatedAttribute('last_name', $locale) }}" title="{{ $player->getTranslatedAttribute('first_name', $locale) }} {{ $player->getTranslatedAttribute('last_name', $locale) }}" />
    </div>
    <div class="top-player__info h-100 flex-grow-1 flex justify-content-start align-items-start flex-wrap p-2">
        <h5 class="text-excelsior-caps text-black fs-16">{{ $player->getTranslatedAttribute('first_name', $locale) }} {{ $player->getTranslatedAttribute('last_name', $locale) }}</h5>
        @unless(is_null($player->school))
        <span class="d-block text-excelsior-exp">{{ trans('website.school') }}: {{ $player->school->getTranslatedAttribute('title', $locale) }}</span>
        @endunless
        @unless(is_null($player->position))
        <span class="d-block text-excelsior-exp text-green">{{ $player->position->getTranslatedAttribute('title', $locale) }}</span>
        @endunless
         
        <div class="star_container" id="" >
           
                <span class="fa fa-star checked" id="first_star"></span>
                <span class="fa fa-star"  id="second_star"></span>
                <span class="fa fa-star"  id="third_star"></span>
                <span class="fa fa-star"  id="fourth_star"></span>
                <span class="fa fa-star"  id="fifth_star"></span>
        
     
        </div>
    </div>
    <div class="top-player__rank h-100 bg-green position-relative d-flex justify-content-between align-items-center flex-column py-3 px-2">
        <span class="text-white text-excelsior-caps opacity-50">{{ trans('website.' . $type) }}</span>
        <span class="top-player__rank__value text-excelsior-caps text-white mb-3">{{ $item['value'] }}</span>
        @if($type === 'rating' && property_exists('goals', $item))
        <div class="top-player__rank__goals text-excelsior-caps w-100 text-center text-white opacity-50 bg-gray">{{ trans('website.goals') }} {{ $item['goals'] }}</div>
        @endif
    </div>
</div>
    </a>

@endif