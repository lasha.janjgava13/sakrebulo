<div class="d-none teams-top d-sm-flex align-items-center w-100 hidden-xs overflow-hidden">
    <div class="container h-100">
        <div class="teams-top__holder w-100 d-flex justify-content-start align-items-baseline h-100 position-relative">
            <span class="teams-top__title mr-3 text-uppercase text-blue text-excelsior-caps">{{ trans('website.teams') }}:</span>
            <div class="teams-top__container d-flex justify-content-start align-items-start flex-nowrap flex-grow overflow-auto pt-2">
                @foreach($teams->shuffle() as $team)
                <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/clubs/' . $team->id) }}" class="teams-top__item text-center">
                    <img
                    class="teams-top__item__image"
                    src="{{ asset('storage/' . $team->logo) }}"
                    srcset="{{ asset('storage/' . $team->logo) }} 2x, {{ asset('storage/' . $team->logo) }} 3x"
                    alt="{{ $team->getTranslatedAttribute('name', $locale) }}"
                    title="{{ $team->getTranslatedAttribute('name', $locale) }}"
                    />
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
