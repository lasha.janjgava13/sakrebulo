      @foreach($items as $menu_item)
      <li class="nav-item">
         <a class="c-menu__navigation__link text-excelsior-caps" target="{{ $menu_item->target }}" {{ $menu_item->target === '_blank' ? 'rel="noopener"' : '' }} href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
      </li>
    @endforeach

