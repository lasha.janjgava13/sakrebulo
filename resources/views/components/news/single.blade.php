@extends('layouts.app-sidebar')

@section('main-block-class', 'main-block--news py-5')

@section('left')


<article class="news-full position-relative z-index-10">
    <h1 class="text-excelsior-caps text-white">{{ $news->getTranslatedAttribute('title', $locale) }}</h1>
    <div class="mb-4">
        @unless(is_null($news->category))
        <a href="#" class="px-2 py-1 text-white text-excelsior-caps bg-success">{{ $news->category->getTranslatedAttribute('name', $locale) }}</a>
        @endunless
        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-newss-center text-white text-excelsior-caps">
            <span class="mx-2">{{ $news->created_at->format('j') }}</span>
            <span>{{ trans('calendar.' . strtolower($news->created_at->format('F'))) }}</span>
        </time>
    </div>
    <img class="news-full__image w-100 object-fit:cover" src="{{ Voyager::image($news->image) }}" alt="{{ $news->getTranslatedAttribute('title', $locale) }}" title="{{ $news->getTranslatedAttribute('title', $locale) }}">
    <div class="my-4">
        {!! $news->body !!}
    </div>
    <div class="col-12">
          <div class="fb-share-button"
        data-href="{{ Request::url() }}"
        data-layout="button_count">
      </div>
    </div>
      <div class="col-12">
          <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
      </div>
    @php
        $array = [
            // '1' => 'images/banner-390x500.jpg',
            '2' => 'images/bog/350 x 450.png',
        ];
        $array_links = [
            // '1' => 'http://sno.ge',
            '2' =>'https://bankofgeorgia.ge/ka/retail/main',
        ];
        $randIndex = array_rand($array, 1);
    @endphp

    <a href="{{$array_links[$randIndex]}}" target="_blank">
        <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
    </a>

    @if($relatedNews->count() > 0)
    <div class="w-100 mt-4">
        <h4 class="text-excelsior-caps">{{ trans('website.related_news') }}</h4>
        <div class="d-flex justify-content-start align-items-start flex-wrap">
            @foreach($relatedNews as $item)
            <div class="related-news w-50 d-flex justify-content-start align-items-start mb-4">
                <div class="w-50 h-100">
                    <img class="w-100 h-100 object-fit:cover" src="{{ Voyager::image($item->image) }}" alt="{{ $item->getTranslatedAttribute('title', $locale) }}" title="{{ $item->getTranslatedAttribute('title', $locale) }}">
                </div>
                <div class="w-50 h-100 px-3 py-4 bg-light-gray">
                    <div class="d-flex justify-content-start align-items-center white-space:nowrap">
                        @unless(is_null($item->category))
                        <a href="#" class="px-2 py-1 font-size-sm text-white text-excelsior-caps bg-success">{{ $item->category->getTranslatedAttribute('name', $locale) }}</a>
                        @endunless
                        <time class="d-inline-flex font-size-sm {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-newss-center text-white text-excelsior-caps">
                            <span class="mx-2">{{ $item->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($item->created_at->format('F'))) }}</span>
                        </time>
                    </div>
                    <h5 class="text-excelsior-caps mt-4">{{ $item->getTranslatedAttribute('title', $locale) }}</h5>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $item->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">მეტი</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</article>
@endsection

@section('right')
    @include('components.sidebar.left-with-ads')
@endsection
