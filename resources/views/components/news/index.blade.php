@extends('layouts.app-sidebar')

@php
$firstNews = $news->shift();
@endphp

@section('container-class', 'py-5')

@section('left')
    <h1 class="text-excelsior-caps mb-4">{{ $pageTitle }}</h1>
    <section>
        @unless(is_null($firstNews))
        <div class="news news--latest w-100">
            <div class="row h-100">
                <div class="col-md-8 col-xs-12 h-100">
                    <div class="row">
                        <img src="{{ Voyager::image($firstNews->image) }}" class="news-image object-fit:contain" alt="{{ $firstNews->getTranslatedAttribute('title', $locale) }}" title="{{ $firstNews->getTranslatedAttribute('title', $locale) }}">
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 h-100 bg-light-gray px-3 py-4">
                    <div class="mb-2">
                        @unless(is_null($firstNews->category))
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">{{ $firstNews->category->getTranslatedAttribute('name', $locale) }}</a>
                        @endunless
                        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-firstNewss-center text-black text-excelsior-caps">
                            <span class="mx-2">{{ $firstNews->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($firstNews->created_at->format('F'))) }}</span>
                        </time>
                        <h4 class="text-excelsior-caps my-3">{{ $firstNews->getTranslatedAttribute('title', $locale) }}</h4>
                        <div class="news__excerpt mb-3">{{ $firstNews->getTranslatedAttribute('exceprt', $locale) }}</div>
                    </div>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $firstNews->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">{{ trans('website.more') }}</a>
                </div>
            </div>
        </div>
        @endunless
        <div class="row">
            @foreach($news as $post)
            <div class="news d-flex justify-content-start align-items-start my-4">
                <div class="col-md-3 col-xs-12 h-100">
                    <img class="w-100 h-100 w-100 object-fit:cover" src="{{ Voyager::image($post->image) }}" alt="{{ $post->getTranslatedAttribute('title', $locale) }}" title="{{ $post->getTranslatedAttribute('title', $locale) }}">
                </div>
                <div class="col-md-9 col-xs-12 h-100">
                    <div class="mb-2">
                        @unless(is_null($post->category))
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">{{ $post->category->getTranslatedAttribute('name', $locale) }}</a>
                        @endunless
                        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-posts-center text-black text-excelsior-caps">
                            <span class="mx-2">{{ $post->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($post->created_at->format('F'))) }}</span>
                        </time>
                        <h4 class="text-excelsior-caps my-3">{{ $post->getTranslatedAttribute('title', $locale) }}</h4>
                        <div class="news__excerpt mb-3">{{ $post->getTranslatedAttribute('exceprt', $locale) }}</div>
                    </div>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $post->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">{{ trans('website.more') }}</a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="mt-3">
            {{ $news->onEachSide(1)->links('vendor.pagination.bootstrap-4') }}
        </div>
    </section>
@endsection

@section('right')
    @include('components.sidebar.left-with-ads')
@endsection
