 @unless(is_null($freeNews))
<article class="news-full position-relative z-index-10 free_news">
    <img class="freeNews-full__image w-100 object-fit:cover" src="{{ Voyager::image($freeNews->image) }}" alt="{{ $freeNews->getTranslatedAttribute('title', $locale) }}" title="{{ $freeNews->getTranslatedAttribute('title', $locale) }}">
    <h1 class="text-excelsior-caps text-red">{{ $freeNews->getTranslatedAttribute('title', $locale) }}</h1>

    <div class="my-4">
        {!! $freeNews->body !!}
    </div>
      <div class="col-12">
          <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
      </div>
</article>
@endunless