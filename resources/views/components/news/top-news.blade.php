@if($news->count() > 0)
@php
    $firstNews = $news->shift();
@endphp
<div class="top-news-block py-4">
    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news') }}">
        <h4 class="top-news-block__title block__title text-dark-blue text-excelsior-caps mb-3">{{ trans('website.main_news') }} &nbsp; <span class="font-size-1">{{ trans('website.all') }}</span></h4>
    </a>
    <div class="row">
        <div class="col-md-6 col-sm-12 dis-xs-none">
            <div class="top-news position-relative">
                <img class="top-news__image w-100 h-100" src="{{ Voyager::image($firstNews->image) }}" alt="{{ $firstNews->getTranslatedAttribute('title', $locale) }}" title="{{ $firstNews->getTranslatedAttribute('title', $locale) }}">
                <div class="top-news__block top-news__block__title d-flex flex-column justify-content-start align-items-start">
                    <div class="mb-2 white-space:nowrap">
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">{{ $firstNews->category->getTranslatedAttribute('name', $locale) }}</a>
                        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-items-center text-black text-excelsior-caps">
                            <span class="mx-2">{{ $firstNews->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($firstNews->created_at->format('F'))) }}</span>
                        </time>
                    </div>
                    <h4 class="top-news__title mb-3 text-excelsior-caps text-black">{{ $firstNews->getTranslatedAttribute('title', $locale) }}</h4>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $firstNews->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">{{ trans('website.more') }}</a>
                </div>
            </div>
        </div>
        @if(count($news) > 0)
        <div class="col-md-6 col-sm-12">
            <div class="dis-sm-none top-news top-news--small d-flex justify-content-start align-items-start">
                <img class="top-news__image top-news__image--small h-100" src="{{ Voyager::image($firstNews->image) }}" alt="{{ $firstNews->getTranslatedAttribute('title', $locale) }}" title="{{ $firstNews->getTranslatedAttribute('title', $locale) }}">
                <div class="top-news__content">
                    <div class="mb-2">
                        @unless(is_null($firstNews->category))
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">{{ $firstNews->category->getTranslatedAttribute('name', $locale) }}</a>
                        @endunless
                        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-items-center text-black text-excelsior-caps">
                            <span class="mx-2">{{ $firstNews->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($firstNews->created_at->format('F'))) }}</span>
                        </time>
                    </div>
                    <h4 class="top-news__title mb-3 text-excelsior-caps text-black">{{ $firstNews->getTranslatedAttribute('title', $locale) }}</h4>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $firstNews->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">მეტი</a>
                </div>
            </div>
            @foreach($news as $item)
            <div class="top-news top-news--small d-flex justify-content-start align-items-start">
                <img class="top-news__image top-news__image--small h-100" src="{{ Voyager::image($item->image) }}" alt="{{ $item->getTranslatedAttribute('title', $locale) }}" title="{{ $item->getTranslatedAttribute('title', $locale) }}">
                <div class="top-news__content">
                    <div class="mb-2">
                        @unless(is_null($item->category))
                        <a href="#" class="px-2 py-1 mr-1 text-white text-excelsior-caps bg-success">{{ $item->category->getTranslatedAttribute('name', $locale) }}</a>
                        @endunless
                        <time class="d-inline-flex {{ $locale === 'en' ? 'flex-row-reverse' : '' }} justify-content-between align-items-center text-black text-excelsior-caps">
                            <span class="mx-2">{{ $item->created_at->format('j') }}</span>
                            <span>{{ trans('calendar.' . strtolower($item->created_at->format('F'))) }}</span>
                        </time>
                    </div>
                    <h4 class="top-news__title mb-3 text-excelsior-caps text-black">{{ $item->getTranslatedAttribute('title', $locale) }}</h4>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, '/news/' . $item->slug) }}" class="top-news__more pb-1 text:hover-dark-blue text-excelsior-caps text-black">მეტი</a>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>
@endif
