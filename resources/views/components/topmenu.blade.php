<nav class="d-none d-sm-flex c-menu__navigation justify-content-start align-items-center w-100 h-50 pl-4">
    @foreach($items as $menu_item)
    <li class="nav-item active">
        <a class="nav-link" target="{{ $menu_item->target }}" {{ $menu_item->target === '_blank' ? 'rel="noopener"' : '' }} href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
      </li>
    @endforeach
</nav>