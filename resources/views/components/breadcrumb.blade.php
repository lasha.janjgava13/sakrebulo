@if(count($links) > 0)
<nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-dot text-excelsior-caps">
        @foreach($links as $link)
        <li class="breadcrumb-item {{ $loop->last ? 'active' : 'text-underline' }}">
            @if(!isset($link->url) || mb_strlen($link->url) === 0)
            {{ $link->title }}
            @else
            <a href="{{ $link->url }}">{{ $link->title }}</a>
            @endif
        </li>
        @endforeach
    </ol>
</nav>
@endif
