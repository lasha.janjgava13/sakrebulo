@extends('layouts.app-sidebar')

@section('main-block-class', 'main-block--news py-5')

@section('left')
<article class="club-full position-relative z-index-10">
    <div class="club-full__head d-flex justify-content-start align-items-center">
        <img class="club-full__icon" src="{{ asset('storage/' . $team->logo) }}" alt="{{ $team->getTranslatedAttribute('name', $locale) }}" title="{{ $team->getTranslatedAttribute('name', $locale) }}" />
        <div class="club-full__head__text d-flex flex-column">
            <h4 class="text-excelsior-caps fs-38 text-white">{{ $team->getTranslatedAttribute('name', $locale) }}</h4>
        </div>
    </div>
    <div class="club-full__body">
        <img  class="club-full__body__image object-fit:contain" src="{{ asset('storage/' . $team->image) }}" alt="{{ $team->getTranslatedAttribute('name', $locale) }}" title="{{ $team->getTranslatedAttribute('name', $locale) }}">
        <h4 class="text-excelsior-caps fs-24">{{ trans('website.players') }}</h4>
        <div class="row">
            @foreach($team->players as $player)
            <div class="col-md-6 club-player--list align-self-center">
                <div class="contain_player d-flex justify-content-start align-items-center w-100 h-100 position-relative">
                    <a class="overlay-link" href="{{ LaravelLocalization::getLocalizedURL($locale, '/player/' . $player->id) }}"></a>
                     <div class="club-player__number">{{ number_format($player->getRating(),1) }}</div>
                    <div class="club-player__full_info">
                        <img class="player__full_info--image h-100 object-fit:contain" src="{{ asset('storage/' . $player->image) }}" alt="{{ $team->getTranslatedAttribute('name', $locale) }}" title="{{ $team->getTranslatedAttribute('name', $locale) }}">
                        <div class="club-player__full_info--text">
                            <div class="text-excelsior-caps name">{{ $player->getTranslatedAttribute('full_name', $locale) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
                @php
                    $array = [
                        // '1' => 'images/banner-800x100.png',
                        '2' => 'images/800x100.png',
                    ];
                    $array_links = [
                        // '1' => 'http://sno.ge',
                        '2' =>'http://psp.ge/new/news/read/303',
                    ];
                    $randIndex = array_rand($array, 1);
                @endphp

<a href="{{$array_links[$randIndex]}}" target="_blank">
    <img src="{{ url($array[$randIndex]) }}" class="img-fluid" alt="banner" title="banner">
</a>
        <div class="col-12">
                  <div class="fb-share-button"
                    data-href="{{ Request::url() }}"
                    data-layout="button_count">
                  </div>
            </div>
        <div class="col-12">
                  <div class="fb-comments" data-href="{{ Request::url() }}" style="width:100%" data-num-posts="3"></div>
              </div>
    </div>
</article>
@endsection

@section('right')
    @include('components.sidebar.left-with-ads')
@endsection
