require('./bootstrap');
require('@fancyapps/fancybox');
import lozad from 'lozad';
import 'slick-carousel';
// const PollPlayer = () => import('./components/PollPlayer.vue');
// const PollTeam = () => import('./components/PollTeam.vue');
import PollPlayer from './components/PollPlayer.vue';
import PollTeam from './components/PollTeam.vue';

window.Vue = require('vue');

const observer = lozad(); // lazy loads elements with default selector as '.lozad'
observer.observe();

// if (document.getElementById('app') !== null) {
//     const app = new Vue({
//         el: '#app',
//         components: {
//             PollPlayer,
//             PollTeam,
//         },
//     });
// }
if (document.getElementById('players-poll') !== null) {
    const playersPollApp = new Vue({
        el: '#players-poll',
        render: (h) => h(PollPlayer),
    });
}
$(document).ready(function () {

    $('.third-button').on('click', function () {

        $('.animated-icon3').toggleClass('open');
        $('body').toggleClass('overflow-hidden');
    });
});
if (document.getElementById('teams-poll') !== null) {
    const TeamsPollApp = new Vue({
        el: '#teams-poll',
        render: (h) => h(PollTeam),
    });
}

if ($('#main-slider').length !== 0) {
    $('#main-slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 10000,
        slidesToShow: 1,
        pauseOnFocus: false,
        arrows: false,
        centerPadding: 0,
        draggable: true,
    });
}

$(window).on('load', () => {

    var items = $('.star_container');
    items.each(function (i, e) {
        let bonus = $(e).data('bonus');
        let raiting = $(e).data('raiting');
        let id = $(e).attr('id');
        id = '#' + id;
        if (raiting > 5 && raiting <= 6) {
            $(id + ' svg#second_star').addClass('checked');
        } else if (raiting > 6 && raiting <= 6.5) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('fa-star-half-alt checked');
        }
        else if (raiting > 6.5 && raiting <= 7) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('checked');
        }
        else if (raiting > 7 && raiting <= 7.5) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('checked');
            $(id + ' svg#fourth_star').addClass('fa-star-half-alt checked');
        }
        else if (raiting > 7.5 && raiting <= 8) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('checked');
            $(id + ' svg#fourth_star').addClass('checked');
        }
        else if (raiting > 8 && raiting <= 9) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('checked');
            $(id + ' svg#fourth_star').addClass('checked');
            $(id + ' svg#fifth_star').addClass('fa-star-half-alt checked');
        }
        else if (raiting > 9 && raiting <= 10) {
            $(id + ' svg#second_star').addClass('checked');
            $(id + ' svg#third_star').addClass('checked');
            $(id + ' svg#fourth_star').addClass('checked');
            $(id + ' svg#fifth_star').addClass('checked');
        }

        if (bonus < 200) {
            $(id + ' svg#fifth_star').addClass('disabled');
        }
        if (bonus < 160) {
            $(id + ' svg#fourth_star').addClass('disabled');
        }
        if (bonus < 120) {
            $(id + ' svg#third_star').addClass('disabled');
        }
        if (bonus < 40) {
            $(id + ' svg#second_star').addClass('disabled');
        }

    })
})
$(document).ready(function () {
    $('.video-widget__link').fancybox({
        buttons: ['close'],
    });
    $('[data-fancybox]').fancybox({
        buttons: ['close'],
    });
    let content = $('.sh-gh .cc').html();
    // setTimeout(function(){
    //     Swal.fire({
    //         title: '<strong><u><a href="https://www.facebook.com/minifootball.com.ge" target="_black">Facebook Page</a></u></strong>',
    //         icon: 'info',
    //         html:
    //           'საქართველოს მინი-ფეხბურთის ასოციაციის ოფიციალური ფეისბუქის გვერდი.<br>'+
    //           content,
    //         showCancelButton: false, // There won't be any cancel button
    //         showConfirmButton: false
    //       })
    //  }, 3000);
    // Swal.fire({
    //     title: '<strong><u><a href="https://www.facebook.com/minifootball.com.ge" target="_black">Facebook Page</a></u></strong>',
    //     icon: 'info',
    //     html:
    //       'საქართველოს მინი-ფეხბურთის ასოციაციის ოფიციალური ფეისბუქის გვერდი.<br>'+
    //       content,
    //     showCancelButton: false, // There won't be any cancel button
    //     showConfirmButton: false,
    //   })
});
