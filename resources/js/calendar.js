import Vue from 'vue';

import Calendar from './components/Calendar.vue';

if (document.getElementById('calendar') !== null) {
    new Vue({
        el: '#calendar',
        render: (h) => h(Calendar),
    });
}
