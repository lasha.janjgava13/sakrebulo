import axios from 'axios';
import swal from 'sweetalert';

export default {
    data() {
        return {
            translations: window.translations,
            poll: {
                title: '',
                answers: [],
            },
            count: 0,
            canVote: true,
        };
    },
    created() {
        if (this.pollId === undefined) {
            return;
        }
        const locale = document.documentElement.lang;
        axios.get(`/api/poll/${this.pollId}?locale=${locale}`).then(({ data: { payload } }) => {
            this.poll.title = payload.item.title;
            this.poll.answers = payload.questions;
            // this.canVote = payload.can;
            this.count = payload.count;
        });
    },
    methods: {
        trans(key) {
            return this.get(key, this.translations);
        },
        get(path, obj) {
            return path.split('.').reduce((prev, curr) => {
                return typeof prev === 'object' && prev[curr] !== undefined ? prev[curr] : curr;
            }, obj);
        },
        vote(answer) {
            if (!this.canVote) {
                return;
            }
            // if (!this.userId) {
            //     swal({
            //         title:this.trans('error'),
            //         text: this.trans('vote_login'),
            //         type:'info',
            //         timer:5000
            //     }).then(function (result) {
            //         console.log(result);
            //       if (result) {
            //         window.location = "/auth/redirect/facebook";
            //       }
            //     });
            //     return;
            // }
            const data = this.generateFormData({
                poll_id: this.pollId,
                // user_id: this.userId,
                answer_id: answer,
            });
            axios.post(`/api/poll`, data).then((response) => {
                this.canVote = false;
                if (response.data.status) {
                    this.recalculateStats(answer);
                } else {
                    swal(this.trans('error'), this.trans('you_already_voted'), 'error');
                }
            });
        },
        redirect(id) {
            window.location.href = '/player/' + id
        },
        recalculateStats(id) {
            this.poll.answers[id].answers_count++;
            let count = 0;
            const answers = this.poll.answers;
            for (let key in answers) {
                count += this.poll.answers[key].answers_count;
            }
            for (let key in answers) {
                answers[key].percent = ((answers[key].answers_count / count) * 100).toFixed(2);
            }
        },
        generateFormData(obj) {
            const data = new FormData();
            for (let key in obj) {
                data.append(key, obj[key]);
            }
            return data;
        },
    },
};
