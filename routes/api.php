<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/calendar', 'ApiController@calendar');
Route::get('/teamplayer/{id}', 'ApiController@teamplayer');
Route::get('/poll/{id}', 'PollStaticController@ajax_getActivePolls');

// Route::group(['middleware' => 'throttle:3,10'], function () {
	Route::post('/poll/', 'PollController@save');
// });
