<?php
use Carbon\Carbon;
use App\Tournament;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'TeamMiddleware']
], function () {
    Route::get('/', 'PageController@home');
    Route::get('/stats', 'PageController@statistics');
    Route::get('/poll', 'PollController@index');
    Route::get('/clubs', 'ClubsController@index');
    Route::get('/clubs/{id}', 'ClubsController@show');
    Route::get('/news', 'NewsController@index');
    Route::get('/news/{slug}', 'NewsController@view');
    Route::get('/calendar', 'PageController@calendar');
    Route::get('/match/{id}', 'PageController@match');
    Route::get('/player/{id}', 'PageController@player');
    Route::get('/standings', 'PageController@standings');
    Route::get('/gallery', 'PageController@gallery');
    // Route::get('/cxrili', 'PageController@home');
    Route::get('/cxrili',function(){
        return view('pages.cxrili');
    });
    Route::get('/standings-dev','PageController@standingsDev');
    Route::get('/test-fl',function(){
         $date = Carbon::now();
        if (config('app.tournament')) {
            $tournament = config('app.tournament');
        } else {
            $tournament = Tournament::whereRaw(
                "end_date >= ? AND start_date <= ?",
                array($date, $date)
            )->first();
        }
        dd($tournament);
    });

});

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/callbeck/facebook/delete',function(){
    echo "user delete comfirmed";
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/matches/manage/{id}', 'CustomController@manageGame')->name('voyager.matches.manage');
    Route::post('/matches/manage/', 'CustomController@postGame');
    Route::get('/polls/activate/{id}', 'PollController@activate');
    Route::get('/reset-statistic-route',function(){
        Artisan::call('command:StatisticCache');
    })->name('reset-statistic-route');
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
